<?php

namespace app\Http\Controllers\KonfirmasiPermintaanBarang;

use app\Models\mBarang;
use app\Models\mBarangKeluar;
use app\Models\mBarangKategori;
use app\Models\mBarangKeluarDetail;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class KonfirmasiPermintaanBarang extends Controller
{
    private $breadcrumb;
    private $menuActive;


    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');
        $this->menuActive = $cons['konfirmasi_permintaan_barang'];
        $this->breadcrumb = [
            [
                'label' => $cons['konfirmasi_permintaan_barang'],
                'route' => route('konfirmasiPermintaanBarangList')
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];


        $data = Main::data($this->breadcrumb);
        $datatable_column = [
            ["data" => "no"],
            ["data" => "faktur"],
            ["data" => "tanggal"],
            ["data" => "status"],
            ["data" => "qty_total"],
            ["data" => "penanggung_jawab"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords
            ),
        ]);

        return view('konfirmasiPermintaanBarang/konfirmasiPermintaanBarangList', $data);
    }


    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mBarangKeluar
            ::whereBetween('brk_tanggal', $where_date);
        if ($keywords) {
            $total_data
                ->whereLike('brk_faktur', $keywords)
                ->orWhereLike('brk_keterangan', $keywords);
        }

        $total_data = $total_data->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_barang_keluar'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mBarangKeluar
            ::select([
                'id_barang_keluar',
                'brk_faktur',
                'brk_tanggal',
                'brk_status',
                'brk_penanggung_jawab'
            ])
            ->whereBetween('brk_tanggal', $where_date);

        if ($keywords) {
            $data_list
                ->whereLike('brk_faktur', $keywords)
                ->orWhereLike('brk_keterangan', $keywords);
        }

        $data_list = $data_list
            ->withCount([
                'barang_keluar_detail AS total_qty_keluar' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(bkd_qty_keluar)'));
                }
            ])
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_barang_keluar = Main::encrypt($row->id_barang_keluar);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['faktur'] = $row->brk_faktur;
            $nestedData['tanggal'] = Main::format_datetime($row->brk_tanggal);
            $nestedData['status'] = $row->brk_status;
            $nestedData['qty_total'] = Main::format_number($row->total_qty_keluar);
            $nestedData['penanggung_jawab'] = $row->brk_penanggung_jawab;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('konfirmasiPermintaanBarangDetailModal', ['id_barang_keluar' => $id_barang_keluar]) . '">
                            <i class="la la-info"></i>
                            Detail
                        </a>
                        <a class="akses-action_wait_done dropdown-item"
                           href="' . route('konfirmasiPermintaanBarangUpdateTerima', ['id_barang_keluar' => $id_barang_keluar]) . '">
                            <i class="la la-info"></i>
                            Terima
                        </a>
                         <a class="akses-action_wait_done dropdown-item"
                           href="' . route('konfirmasiPermintaanBarangUpdateTolak', ['id_barang_keluar' => $id_barang_keluar]) . '">
                            <i class="la la-info"></i>
                            Tolak
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'keywords' => $keywords
        );

        return $json_data;
    }

    function detail_modal($id_barang_keluar)
    {
        $id_barang_keluar = Main::decrypt($id_barang_keluar);
        $barang_keluar = mBarangKeluar
            ::with([
                'user',
                'barang_keluar_detail',
                'barang_keluar_detail.barang',
            ])
            ->where('id_barang_keluar', $id_barang_keluar)
            ->first();

        $data = [
            'barang_keluar' => $barang_keluar,
        ];

        return view('konfirmasiPermintaanBarang.konfirmasiPermintaanBarangDetailModal', $data);
    }

    function update_terima($id_barang_keluar)
    {
        $id_barang_keluar = Main::decrypt($id_barang_keluar);
        $data = [
            'brk_status' => 'terima'
        ];
        mBarangKeluar::where(['id_barang_keluar' => $id_barang_keluar])->update($data);

        $barang_keluar_detail = mBarangKeluarDetail::where('id_barang_keluar', $id_barang_keluar)->get();

        foreach ($barang_keluar_detail as $row) {
            $barang = mBarang::where('id_barang', $row->id_barang)->first();
            $brg_stok_now = $barang->brg_stok - $row->bkd_qty_keluar;

            mBarang::where('id_barang', $row->id_barang)->update(['brg_stok' => $brg_stok_now, 'updated_at' => date('Y-m-d H:i:s')]);
        }


        return redirect()->route('konfirmasiPermintaanBarangList');
    }

    function update_tolak($id_barang_keluar)
    {
        $id_barang_keluar = Main::decrypt($id_barang_keluar);
        $data = [
            'brk_status' => 'tolak'
        ];
        mBarangKeluar::where(['id_barang_keluar' => $id_barang_keluar])->update($data);

        return redirect()->route('konfirmasiPermintaanBarangList');
    }
}
