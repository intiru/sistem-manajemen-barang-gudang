<?php

namespace app\Http\Controllers\LaporanBarangKeluar;

use app\Models\mBarang;
use app\Models\mBarangKeluar;
use app\Models\mBarangKeluarDetail;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LaporanBarangKeluar extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');

        $this->menuActive = $cons['laporan_barang_keluar'];
        $this->breadcrumb = [
            [
                'label' => $cons['laporan_barang_keluar'],
                'route' => route('laporanBarangKeluarList')
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request, ['date']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];

        $data = Main::data($this->breadcrumb);

        $nominal = $this->nominal($date_from_db, $date_to_db);
        $barang_keluar = $nominal['barang_keluar'];



        $datatable_column_barang_keluar = [
            ["data" => "no"],
            ["data" => "faktur"],
            ["data" => "nama_barang"],
            ["data" => "tanggal"],
            ["data" => "status"],
            ["data" => "keterangan"],
            ["data" => "qty_keluar"],
        ];

        $data = array_merge($data, [
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db
            ),

            'datatable_column_barang_keluar' => $datatable_column_barang_keluar,
            'barang_keluar_total' => $barang_keluar,
            'date_filter' => $date_filter
        ]);

        return view('laporan/laporanBarangKeluar/laporanBarangKeluarList', $data);
    }

    function data_table_barang_keluar(Request $request)
    {
        $data_post = $request->input('data');
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mBarangKeluarDetail
            ::join('barang_keluar', 'barang_keluar.id_barang_keluar', 'barang_keluar_detail.id_barang_keluar')
            ->whereBetween('brk_tanggal', $where_date)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_barang_keluar_detail'; //$columns[$request->input('order.0.column')];
//        $order_type = $request->input('order.0.dir');
        $order_type = 'asc';

        $data_list = mBarangKeluarDetail
            ::join('barang_keluar', 'barang_keluar.id_barang_keluar', 'barang_keluar_detail.id_barang_keluar')
            ->leftJoin('barang', 'barang.id_barang', 'barang_keluar_detail.id_barang')
            ->whereBetween('brk_tanggal', $where_date)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['faktur'] = $row->brk_faktur;
            $nestedData['tanggal'] = Main::format_datetime($row->brk_tanggal);
            $nestedData['nama_barang'] = $row->brg_kode . ' ' . $row->brg_nama;
            $nestedData['status'] = Main::barang_tipe($row->brk_tipe);
            $nestedData['keterangan'] = $row->brk_keterangan;
            $nestedData['qty_keluar'] = Main::format_number($row->bkd_qty_keluar);

            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
        );

        return $json_data;
    }

    function cetak_pdf(Request $request)
    {

        $data_post = $request->input('data');
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $date_from_label = Main::format_date_label($data_post['date_from_db']);
        $date_to_label = Main::format_date_label($data_post['date_to_db']);

        $nominal = $this->nominal($date_from_db, $date_to_db);
        $barang_keluar = $nominal['barang_keluar'];


        $data_barang_keluar = mBarangKeluarDetail
            ::join('barang_keluar', 'barang_keluar.id_barang_keluar', 'barang_keluar_detail.id_barang_keluar')
            ->leftJoin('barang', 'barang.id_barang', 'barang_keluar_detail.id_barang')
            ->whereBetween('brk_tanggal', $where_date)
            ->orderBy('id_barang_keluar_detail', 'ASC')
            ->get();

        $kepala_gudang_nama = mUser::where('id_user_role', '25')->value('nama_karyawan');
        $manager_nama = mUser::where('id_user_role', '24')->value('nama_karyawan');

        $data = [
            'date_from' => $date_from_label,
            'date_to' => $date_to_label,
            'barang_keluar' => $barang_keluar,
            'data_barang_keluar' => $data_barang_keluar,
            'kepala_gudang_nama' => $kepala_gudang_nama,
            'manager_nama' => $manager_nama
        ];

        return [
            'view' => (string) view('laporan/laporanBarangKeluar/laporanBarangKeluarPdf', $data)
        ];
    }

    function nominal($date_from_db, $date_to_db) {

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $barang_keluar = mBarangKeluarDetail
            ::leftJoin('barang_keluar', 'barang_keluar.id_barang_keluar', '=', 'barang_keluar_detail.id_barang_keluar')
            ->whereBetween('brk_tanggal', $where_date)
            ->sum('bkd_qty_keluar');

        return [
            'barang_keluar' => $barang_keluar,
        ];
    }

}
