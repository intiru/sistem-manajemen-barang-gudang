<?php

namespace app\Http\Controllers\User;

use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Hash;
use app\Rules\UsernameChecker;
use app\Rules\UsernameCheckerUpdate;
use Illuminate\Support\Facades\Config;

use app\Models\mKaryawan;
use app\Models\mUser;
use app\Models\mUserRole;

class User extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['user'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $user_role = mUserRole::orderBy('role_name', 'ASC')->get();
        $user = mUser::with(['user_role'])->orderBy('username', 'ASC')->get();

        $data = array_merge($data, [
            'user_role' => $user_role,
            'data' => $user
        ]);

        return view('user/userList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'id_user_role' => 'required',
            'username' => ['required', new UsernameChecker],
            'password' => 'required',
            'nama_karyawan' => 'required',
            'alamat_karyawan' => 'required',
            'email_karyawan' => '',
        ]);

        $data = $request->except('_token');
        if ($request->hasFile('foto_karyawan')) {
            $file = $request->file('foto_karyawan');
            $file->move('upload', $file->getClientOriginalName());
            $data['foto_karyawan'] = $file->getClientOriginalName();
        }


        $data['password'] = Hash::make($data['password']);
        mUser::create($data);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $user = mUser::where('id', $id)->first();
        $user_role = mUserRole::orderBy('role_name', 'ASC')->get();
        $data = [
            'edit' => $user,
            'user_role' => $user_role
        ];

        return view('user/userEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mUser::where('id', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'id_user_role' => 'required',
            'username' => ['required', new UsernameCheckerUpdate($id)],
            'nama_karyawan' => 'required',
            'alamat_karyawan' => 'required',
            //'password' => 'required'
        ]);
        $data = $request->except("_token");
        if ($request->input('password')) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }
        if ($request->hasFile('foto_karyawan')) {
            $file = $request->file('foto_karyawan');
            $file->move('upload', $file->getClientOriginalName());
            $data['foto_karyawan'] = $file->getClientOriginalName();
        }
        mUser::where(['id' => $id])->update($data);
    }
}
