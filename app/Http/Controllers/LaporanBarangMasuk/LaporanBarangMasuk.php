<?php

namespace app\Http\Controllers\LaporanBarangMasuk;

use app\Models\mBarang;
use app\Models\mBarangMasuk;
use app\Models\mBarangMasukDetail;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LaporanBarangMasuk extends Controller
{
    private $breadcrumb;
    private $menuActive;
    private $ppnPersen;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');

        $this->menuActive = $cons['laporan_barang_masuk'];
        $this->breadcrumb = [
            [
                'label' => $cons['laporan_barang_masuk'],
                'route' => route('laporanBarangMasukList')
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request, ['date']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];

        $data = Main::data($this->breadcrumb);

        $nominal = $this->nominal($date_from_db, $date_to_db);
        $barang_masuk = $nominal['barang_masuk'];



        $datatable_column_barang_masuk = [
            ["data" => "no"],
            ["data" => "faktur"],
            ["data" => "nama_barang"],
            ["data" => "tanggal"],
            ["data" => "keterangan"],
            ["data" => "qty_masuk"],
        ];

        $data = array_merge($data, [
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db
            ),

            'datatable_column_barang_masuk' => $datatable_column_barang_masuk,
            'barang_masuk_total' => $barang_masuk,
            'date_filter' => $date_filter
        ]);

        return view('laporan/laporanBarangMasuk/laporanBarangMasukList', $data);
    }

    function data_table_barang_masuk(Request $request)
    {
        $data_post = $request->input('data');
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mBarangMasukDetail
            ::join('barang_masuk', 'barang_masuk.id_barang_masuk', 'barang_masuk_detail.id_barang_masuk')
            ->whereBetween('brm_tanggal', $where_date)
            ->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_barang_masuk_detail'; //$columns[$request->input('order.0.column')];
//        $order_type = $request->input('order.0.dir');
        $order_type = 'asc';

        $data_list = mBarangMasukDetail
            ::join('barang_masuk', 'barang_masuk.id_barang_masuk', 'barang_masuk_detail.id_barang_masuk')
            ->leftJoin('barang', 'barang.id_barang', 'barang_masuk_detail.id_barang')
            ->whereBetween('brm_tanggal', $where_date)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['faktur'] = $row->brm_faktur;
            $nestedData['tanggal'] = Main::format_datetime($row->brm_tanggal);
            $nestedData['nama_barang'] = $row->brg_kode . ' ' . $row->brg_nama;
            $nestedData['keterangan'] = $row->brm_keterangan;
            $nestedData['qty_masuk'] = Main::format_number($row->bmd_qty_masuk);

            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
        );

        return $json_data;
    }

    function cetak_pdf(Request $request)
    {

        $data_post = $request->input('data');
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $date_from_label = Main::format_date_label($data_post['date_from_db']);
        $date_to_label = Main::format_date_label($data_post['date_to_db']);

        $nominal = $this->nominal($date_from_db, $date_to_db);
        $barang_masuk = $nominal['barang_masuk'];


        $data_barang_masuk = mBarangMasukDetail
            ::join('barang_masuk', 'barang_masuk.id_barang_masuk', 'barang_masuk_detail.id_barang_masuk')
            ->leftJoin('barang', 'barang.id_barang', 'barang_masuk_detail.id_barang')
            ->whereBetween('brm_tanggal', $where_date)
            ->orderBy('id_barang_masuk_detail', 'ASC')
            ->get();

        $admin_nama = mUser::where('id_user_role', '2')->value('nama_karyawan');
        $manager_nama = mUser::where('id_user_role', '24')->value('nama_karyawan');

        $data = [
            'date_from' => $date_from_label,
            'date_to' => $date_to_label,
            'barang_masuk' => $barang_masuk,
            'data_barang_masuk' => $data_barang_masuk,
            'admin_nama' => $admin_nama,
            'manager_nama' => $manager_nama
        ];

        return [
            'view' => (string) view('laporan/laporanBarangMasuk/laporanBarangMasukPdf', $data)
        ];
    }

    function nominal($date_from_db, $date_to_db) {

        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $barang_masuk = mBarangMasukDetail
            ::leftJoin('barang_masuk', 'barang_masuk.id_barang_masuk', '=', 'barang_masuk_detail.id_barang_masuk')
            ->whereBetween('brm_tanggal', $where_date)
            ->sum('bmd_qty_masuk');

        return [
            'barang_masuk' => $barang_masuk,
        ];
    }

}
