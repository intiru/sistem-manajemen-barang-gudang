<?php

namespace app\Http\Controllers\PermintaanBarang;

use app\Models\mBarang;
use app\Models\mBarangKeluar;
use app\Models\mBarangKategori;
use app\Models\mBarangKeluarDetail;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PermintaanBarang extends Controller
{
    private $breadcrumb;
    private $menuActive;


    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');
        $this->menuActive = $cons['permintaan_barang'];
        $this->breadcrumb = [
            [
                'label' => $cons['permintaan_barang'],
                'route' => route('permintaanBarangList')
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];


        $data = Main::data($this->breadcrumb);
        $datatable_column = [
            ["data" => "no"],
            ["data" => "faktur"],
            ["data" => "tanggal"],
//            ["data" => "status"],
            ["data" => "qty_total"],
            ["data" => "penanggung_jawab"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords
            ),
        ]);

        return view('permintaanBarang/permintaanBarangList', $data);
    }


    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mBarangKeluar
            ::whereBetween('brk_tanggal', $where_date)
            ->where('brk_status', 'pengajuan');
        if ($keywords) {
            $total_data
                ->whereLike('brk_faktur', $keywords)
                ->orWhereLike('brk_keterangan', $keywords);
        }

        $total_data = $total_data->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_barang_keluar'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mBarangKeluar
            ::select([
                'id_barang_keluar',
                'brk_faktur',
                'brk_tanggal',
                'brk_status',
                'brk_penanggung_jawab'
            ])
            ->where('brk_status', 'pengajuan')
            ->whereBetween('brk_tanggal', $where_date);

        if ($keywords) {
            $data_list
                ->whereLike('brk_faktur', $keywords)
                ->orWhereLike('brk_keterangan', $keywords);
        }

        $data_list = $data_list
            ->withCount([
                'barang_keluar_detail AS total_qty_keluar' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(bkd_qty_keluar)'));
                }
            ])
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_barang_keluar = Main::encrypt($row->id_barang_keluar);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['faktur'] = $row->brk_faktur;
            $nestedData['tanggal'] = Main::format_datetime($row->brk_tanggal);
            $nestedData['status'] = $row->brk_status;
            $nestedData['qty_total'] = Main::format_number($row->total_qty_keluar);
            $nestedData['penanggung_jawab'] = $row->brk_penanggung_jawab;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_done dropdown-item"
                           href="' . route('permintaanBarangEdit', ['id_barang_keluar' => $id_barang_keluar]) . '">
                            <i class="la la-edit"></i>
                            Edit
                        </a>
                        <a class="akses-action_wait_done btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('permintaanBarangDetailModal', ['id_barang_keluar' => $id_barang_keluar]) . '">
                            <i class="la la-info"></i>
                            Detail
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'keywords' => $keywords
        );

        return $json_data;
    }

    function create()
    {
        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Barang Keluar',
                    'route' => ''
                ]
            ]
        );
        $data = Main::data($breadcrumb, $this->menuActive);
        $barang = mBarang::orderBy('brg_kode')->get();
        $id_barang_keluar = mBarangKeluar::orderBy('id_barang_keluar', 'DESC')->value('id_barang_keluar');
        $no_faktur = 'FKR-'.$id_barang_keluar;


        $data = array_merge(
            $data, array(
                'barang' => $barang,
                'no_faktur' => $no_faktur
            )
        );

        return view('permintaanBarang/permintaanBarangCreate', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'brk_faktur' => 'required',
            'brk_penanggung_jawab' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'qty_keluar' => 'required',
            'qty_keluar.*' => 'required',
        ];

        $attributes = [
            'id_barang' => 'Barang',
            'qty_keluar' => 'Qty Barang keluar',

        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['qty_keluar.' . $i] = 'Qty Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_barang_arr = $request->input('id_barang');

        $brk_faktur = $request->input('brk_faktur');
        $brk_penanggung_jawab = $request->input('brk_penanggung_jawab');
        $brk_tanggal = Main::format_datetime_db($request->input('tanggal'));
        $brk_keterangan = $request->input('keterangan');
        $brk_status = 'pengajuan';
        $brk_tipe = $request->input('tipe');

        $bkd_qty_keluar_arr = $request->input('qty_keluar');


        $created_at = date('Y-m-d H:i:s');
        $updated_at = $created_at;


        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke barang keluar data
             */
            $barang_keluar_data = [
                'id_user' => $id_user,
                'brk_faktur' => $brk_faktur,
                'brk_tanggal' => $brk_tanggal,
                'brk_keterangan' => $brk_keterangan,
                'brk_status' => $brk_status,
                'brk_tipe' => $brk_tipe,
                'brk_penanggung_jawab' => $brk_penanggung_jawab,
            ];

            $response_barang_keluar = mBarangKeluar::create($barang_keluar_data);
            $id_barang_keluar = $response_barang_keluar->id_barang_keluar;

            foreach ($id_barang_arr as $index => $id_barang) {

                /**
                 * Memasukkan data ke barang keluar detail
                 */

                $bkd_qty_keluar = Main::format_number_db($bkd_qty_keluar_arr[$index]);


                $barang_keluar_detail_data = [
                    'id_barang_keluar' => $id_barang_keluar,
                    'id_barang' => $id_barang,
                    'bkd_qty_keluar' => $bkd_qty_keluar,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at
                ];

                $response_barang_keluar_detail = mBarangKeluarDetail::create($barang_keluar_detail_data);
                $id_barang_keluar_detail = $response_barang_keluar_detail->id_barang_keluar_detail;

            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function edit($id_barang_keluar)
    {
        $id_barang_keluar = Main::decrypt($id_barang_keluar);

        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Edit Barang Keluar',
                    'route' => ''
                ]
            ]
        );

        $data = Main::data($breadcrumb, $this->menuActive);
        $barang = mBarang::orderBy('brg_kode')->get();
        $edit = mBarangKeluar
            ::with([
                'barang_keluar_detail',
                'barang_keluar_detail.barang',
            ])
            ->where('id_barang_keluar', $id_barang_keluar)
            ->first();

        $data = array_merge(
            $data, array(
                'barang' => $barang,
                'edit' => $edit
            )
        );

        return view('permintaanBarang/permintaanBarangEdit', $data);
    }

    function update(Request $request, $id_barang_keluar)
    {
        $rules = [
            'brk_faktur' => 'required',
            'brk_penanggung_jawab' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'qty_keluar' => 'required',
            'qty_keluar.*' => 'required',
        ];

        $attributes = [
            'id_barang' => 'Barang',
            'qty_keluar' => 'Qty Barang keluar',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['qty_keluar.' . $i] = 'Qty Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_barang_keluar = Main::decrypt($id_barang_keluar);
        $id_barang_keluar_detail_arr = $request->input('id_barang_keluar_detail');
        $brk_faktur = $request->input('brk_faktur');

        $brk_tanggal = Main::format_datetime_db($request->input('tanggal'));
        $brk_tipe = $request->input('tipe');
        $brk_keterangan = $request->input('keterangan');
        $brk_penanggung_jawab = $request->input('brk_penanggung_jawab');

        $bkd_qty_keluar_arr = $request->input('qty_keluar');

        $barang_keluar = mBarangKeluar::where('id_barang_keluar', $id_barang_keluar)->first();


        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke barang keluar data
             */
            $barang_keluar_data = [
                'brk_faktur' => $brk_faktur,
                'brk_tanggal' => $brk_tanggal,
                'brk_keterangan' => $brk_keterangan,
                'brk_tipe' => $brk_tipe,
                'brk_penanggung_jawab' => $brk_penanggung_jawab,
            ];

            mBarangKeluar::where('id_barang_keluar', $id_barang_keluar)->update($barang_keluar_data);

            foreach ($id_barang_keluar_detail_arr as $index => $id_barang_keluar_detail) {

                /**
                 * Memperbarui data ke barangkeluar detail
                 */


                $barang_keluar_detail_data = [


                ];

                mBarangKeluarDetail::where('id_barang_keluar_detail', $id_barang_keluar_detail)->update($barang_keluar_detail_data);

            }
            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }

//        asdfasdf
    }

    function detail_modal($id_barang_keluar)
    {
        $id_barang_keluar = Main::decrypt($id_barang_keluar);
        $barang_keluar = mBarangKeluar
            ::with([
                'user',
                'barang_keluar_detail',
                'barang_keluar_detail.barang',
            ])
            ->where('id_barang_keluar', $id_barang_keluar)
            ->first();

        $data = [
            'barang_keluar' => $barang_keluar,
        ];

        return view('permintaanBarang.permintaanBarangDetailModal', $data);
    }
}
