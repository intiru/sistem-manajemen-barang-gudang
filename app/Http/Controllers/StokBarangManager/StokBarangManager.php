<?php

namespace app\Http\Controllers\StokBarangManager;

use app\Models\mBarang;
use app\Models\mBarangKategori;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;

class StokBarangManager extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['stok_barang_manager'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request, ['keywords']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];


        $data = Main::data($this->breadcrumb);
        $data_list = DB::table('barang')
            ->leftJoin('barang_kategori', 'barang_kategori.id_barang_kategori', '=', 'barang.id_barang_kategori')
            ->get();
        $barang_kategori = mBarangKategori::orderBy('brk_kode', 'ASC')->get();
        $datatable_column = [
            ["data" => "no"],
            ["data" => "brk_kode"],
            ["data" => "brg_kode"],
            ["data" => "brg_nama"],
            ["data" => "brg_stok"],
            ["data" => "brg_satuan"],
        ];

        $data = array_merge($data, [
            'data' => $data_list,
            'barang_kategori' => $barang_kategori,
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords
            ),
        ]);

        return view('stokBarangManager/stokBarangManagerList', $data);
    }


    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];

        if ($keywords) {
            $total_data = mBarang
                ::whereLike('id_barang', $keywords)
                ->orWhereLike('brg_nama', $keywords)
                ->count();
        } else {
            $total_data = mBarang
                ::count();
        }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_barang'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mBarang
            ::leftJoin('barang_kategori', 'barang_kategori.id_barang_kategori', '=', 'barang.id_barang_kategori');
        if ($keywords) {
            $data_list = $data_list
                ->whereLike('id_barang', $keywords)
                ->orWhereLike('brg_nama', $keywords);
        }

        $data_list = $data_list
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_barang = Main::encrypt($row->id_barang);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['brk_kode'] = $row->brk_kode;
            $nestedData['brg_kode'] = $row->brg_kode;
            $nestedData['brg_nama'] = $row->brg_nama;
            $nestedData['brg_stok'] = Main::format_number($row->brg_stok);
            $nestedData['brg_satuan'] = $row->brg_satuan;


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'keywords' => $keywords
        );

        return $json_data;
    }

}
