<?php

namespace app\Http\Controllers\General;

use app\Http\Controllers\Controller;
use app\Models\mUser;
use app\Models\mUserRole;
use app\Rules\LoginCheck;
use app\Rules\LoginRolesCheck;
use app\Rules\UsernameChecker;
use app\Rules\UsernameExist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class Login extends Controller
{
    function index()
    {
        $data = [
            'pageTitle' => 'Login | ' . env('APP_NAME'),
            'roles' => mUserRole::orderBy('role_name', 'ASC')->get()
        ];

        return view('general/login', $data);
    }

    function lupaPassword()
    {
        $data = [
            'pageTitle' => 'Lupa Password | ' . env('APP_NAME'),
            'roles' => mUserRole::orderBy('role_name', 'ASC')->get()
        ];

        return view('general/lupaPassword', $data);
    }

    function lupaPasswordDo(Request $request)
    {

        $request->validate([
            'username' => ['required',  new UsernameExist()],
            'password' => ['required', 'required_with:password_confirm'],
            'password_confirm' => ['required', 'same:password'],
        ]);

        $username = $request->input('username');
        $password = $request->input('password');

        mUser::where('username', $username)->update(['password' => Hash::make($password)]);

    }

    function do(Request $request)
    {

        $request->validate([
            'username' => 'required',
            'password' => ['required', new LoginCheck($request->username)]
        ]);

//        return response(422, []);

    }

    function roles(Request $request)
    {

        $request->validate([
            'username' => 'required',
            'password' => ['required', new LoginRolesCheck($request->username)]
        ]);
    }
}
