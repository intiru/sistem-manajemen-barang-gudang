<?php

namespace app\Http\Controllers\General;

use app\Models\mBarang;
use app\Models\mBarangKategori;
use app\Models\mBarangMasuk;
use app\Models\mBarangMasukDetail;
use app\Models\mKaryawan;
use app\Models\mUser;
use app\Models\mUserRole;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Session;



class DashboardAdmin extends Controller
{
    private $breadcrumb = [
        [
            'label' => 'dashboard_admin',
            'route' => ''
        ]
    ];

    private $bulan = [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'Nopember',
        '12' => 'Desember',
    ];

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];


        $data = Main::data($this->breadcrumb);
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_barang = mBarang
            ::where('id_barang', '>=', '1')
            ->sum('brg_stok');
        $total_barang_kategori = mBarangKategori
            ::where('id_barang_kategori', '>=', '1')
            ->count();
        $total_karyawan = 0;
        $total_user = mUser
            ::where('id', '>=', '1')
            ->count();
        $total_role_user = mUserRole
            ::where('id', '>=', '1')
            ->count();
        $total_barang_masuk = mBarangMasukDetail::whereBetween('created_at', $where_date)->sum('bmd_qty_masuk');

        $start = new \DateTime($date_from_db);
        $end = new \DateTime($date_to_db);
        $end = $end->modify('+1 day');
        $interval = new \DateInterval('P1D');

        $label = [];
        $period = new \DatePeriod($start, $interval, $end);

        foreach ($period as $key => $value) {
            $label[] = $value->format('Y-m-d');
        }


        $cart_patient = [
            'label' => $label
        ];

        foreach ($period as $key => $value) {
            $date = $value->format('Y-m-d');

            $cart_patient['data']['appointment'][$date] = 0;

            $cart_patient['data']['consult'][$date] = 0;

            $cart_patient['data']['action'][$date] = 0;

            $cart_patient['data']['control'][$date] = 0;

            $cart_patient['data']['patient'][$date] = 0;

            $cart_patient['data']['payment'][$date] = 0;
        }


        $data = array_merge($data, array(
            'total_barang' => Main::format_number($total_barang),
            'total_barang_kategori' => Main::format_number($total_barang_kategori),
            'total_karyawan' => Main::format_number($total_karyawan),
            'total_user' => Main::format_number($total_user),
            'total_role_user' => Main::format_number($total_role_user),
            'total_barang_masuk' => Main::format_number($total_barang_masuk),
            'cart_patient' => $cart_patient,
            'date_filter' => $date_filter
        ));

        return view('dashboard/dashboard_admin', $data);

    }

    function whatsapp_test()
    {
        Main::whatsappSend('+6281934364063', 'HELLO,, ini adalah test message ' . date('d-m-Y H:i:s'));
    }


}
