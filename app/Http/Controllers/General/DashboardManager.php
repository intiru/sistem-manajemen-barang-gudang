<?php

namespace app\Http\Controllers\General;

use app\Models\mBarang;
use app\Models\mBarangKategori;
use app\Models\mBarangKeluar;
use app\Models\mBarangMasuk;
use app\Models\mBarangMasukDetail;
use app\Models\mBarangKeluarDetail;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;


use DB;
use Illuminate\Support\Facades\Session;



class DashboardManager extends Controller
{
    private $breadcrumb = [
        [
            'label' => 'dashboard_manager',
            'route' => ''
        ]
    ];

    private $bulan = [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'Nopember',
        '12' => 'Desember',
    ];

    function index(Request $request)
    {
//        return Session::all();
        $data = $this->data_dashboard_manager($request);

//        return $data['cart_patient'];

        return view('dashboard/dashboard_manager', $data);

    }

    function data_dashboard_manager($request)
    {

        $filter_component = Main::date_filter($request);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];


        $data = Main::data($this->breadcrumb);
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_barang = mBarang::sum('brg_stok');
        $total_stok_barang = mBarang
            ::sum('brg_stok');
        $total_barang_masuk = mBarangMasukDetail
            ::leftJoin('barang_masuk', 'barang_masuk.id_barang_masuk', '=', 'barang_masuk_detail.id_barang_masuk')
            ->whereBetween('brm_tanggal', $where_date)
            ->sum('bmd_qty_masuk');
        $total_konfirmasi_permintaan_barang = mBarangKeluar
            ::whereBetween('brk_tanggal', $where_date)
            ->where('brk_status', '=', 'pengajuan')
            ->count();
        $total_laporan_barang_masuk = mBarangMasukDetail
            ::leftJoin('barang_masuk', 'barang_masuk.id_barang_masuk', '=', 'barang_masuk_detail.id_barang_masuk')
            ->whereBetween('brm_tanggal', $where_date)
            ->sum('bmd_qty_masuk');
        $total_laporan_barang_keluar = mBarangKeluarDetail
            ::leftJoin('barang_keluar', 'barang_keluar.id_barang_keluar', '=', 'barang_keluar_detail.id_barang_keluar')
            ->whereBetween('brk_tanggal', $where_date)
            ->sum('bkd_qty_keluar');

        $start = new \DateTime($date_from_db);
        $end = new \DateTime($date_to_db);
        $end = $end->modify('+1 day');
        $interval = new \DateInterval('P1D');

        $label = [];
        $period = new \DatePeriod($start, $interval, $end);

        foreach ($period as $key => $value) {
            $label[] = $value->format('Y-m-d');
        }


        $cart_patient = [
            'label' => $label
        ];

        foreach ($period as $key => $value) {
            $date = $value->format('Y-m-d');

            $cart_patient['data']['appointment'][$date] = 0;

            $cart_patient['data']['consult'][$date] = 0;

            $cart_patient['data']['action'][$date] = 0;

            $cart_patient['data']['control'][$date] = 0;

            $cart_patient['data']['patient'][$date] = 0;

            $cart_patient['data']['payment'][$date] = 0;
        }


        $data = array_merge($data, array(
            'total_barang' => $total_barang,
            'total_stok_barang' => Main::format_number($total_stok_barang),
            'total_barang_masuk' => Main::format_number($total_barang_masuk),
            'total_konfirmasi_permintaan_barang' => Main::format_number($total_konfirmasi_permintaan_barang),
            'total_laporan_barang_masuk' => Main::format_number($total_laporan_barang_masuk),
            'total_laporan_barang_keluar' => Main::format_number($total_laporan_barang_keluar),
            'cart_patient' => $cart_patient,
            'date_filter' => $date_filter
        ));
        return $data;
    }

    function whatsapp_test()
    {
        Main::whatsappSend('+6281934364063', 'HELLO,, ini adalah test message ' . date('d-m-Y H:i:s'));
    }


}
