<?php

namespace app\Http\Controllers\General;

use app\Models\mDistributor;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use app\Models\mKaryawan;
use app\Models\mUser;

use app\Rules\UsernameCheckerUpdate;
use app\Rules\UsernameDistributorCheckerUpdate;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class Profile extends Controller
{

    function index()
    {
        $breadcrumb = [
            [
                'label' => 'Profil',
                'route' => ''
            ]
        ];
        $data = Main::data($breadcrumb);
        return view('general/profile_administrator', $data);
    }

    function update_administrator(Request $request)
    {
        $user = Session::get('user');
        $id = $user->id;

        $request->validate([
            'username' => ['bail', 'required', new UsernameCheckerUpdate($id)],
            'password' => 'bail',
            'password_confirm' => 'bail|same:password',
            'nama_karyawan' => 'required',
            'alamat_karyawan' => 'required',
            'email_karyawan' => 'bail|email'
        ]);

        $data_user = $request->except("_token");
        unset($data_user['password_confirm']);
        if ($request->filled('password')) {
            $data_user['password'] = Hash::make($request->input('password'));
        }

        if ($request->hasFile('foto_karyawan')) {
            $file = $request->file('foto_karyawan');
            $file->move('upload', $file->getClientOriginalName());
            $data_user['foto_karyawan'] = $file->getClientOriginalName();
        }

        mUser::where('id', $id)->update($data_user);

        $user = mUser::where('id', $id)->first();
        Session::put(['user' => $user]);

    }
}
