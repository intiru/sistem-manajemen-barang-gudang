<?php

namespace app\Http\Controllers\BarangMasuk;

use app\Models\mBarang;
use app\Models\mBarangMasuk;
use app\Models\mBarangKategori;
use app\Models\mBarangMasukDetail;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BarangMasuk extends Controller
{
    private $breadcrumb;
    private $menuActive;


    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $cons_top = Config::get('constants');
        $this->menuActive = $cons['barang_masuk'];
        $this->breadcrumb = [
            [
                'label' => $cons['barang_masuk'],
                'route' => route('barangMasukList')
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];


        $data = Main::data($this->breadcrumb);
        $datatable_column = [
            ["data" => "no"],
            ["data" => "faktur"],
            ["data" => "tanggal"],
            ["data" => "qty_total"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords
            ),
        ]);

        return view('barangMasuk/barangMasukList', $data);
    }


    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];
        $date_from_db = $data_post['date_from_db'];
        $date_to_db = $data_post['date_to_db'];
        $where_date = [$date_from_db . " 00:00:00", $date_to_db . " 23:59:59"];

        $total_data = mBarangMasuk
            ::whereBetween('brm_tanggal', $where_date);
        if ($keywords) {
            $total_data
                ->whereLike('brm_faktur', $keywords)
                ->orWhereLike('brm_keterangan', $keywords);
        }

        $total_data = $total_data->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_barang_masuk'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        $data_list = mBarangMasuk
            ::select([
                'id_barang_masuk',
                'brm_faktur',
                'brm_tanggal',
            ])
            ->whereBetween('brm_tanggal', $where_date);

        if ($keywords) {
            $data_list
                ->whereLike('brm_faktur', $keywords)
                ->orWhereLike('brm_keterangan', $keywords);
        }

        $data_list = $data_list
            ->withCount([
                'barang_masuk_detail AS total_qty_masuk' => function ($query) {
                    $query
                        ->select(DB::raw('SUM(bmd_qty_masuk)'));
                }
            ])
            ->offset($start)
            ->limit($limit)
            ->orderBy($order_column, $order_type)
            ->get();

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_barang_masuk = Main::encrypt($row->id_barang_masuk);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['faktur'] = $row->brm_faktur;
            $nestedData['tanggal'] = Main::format_datetime($row->brm_tanggal);
            $nestedData['qty_total'] = Main::format_number($row->total_qty_masuk);
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-edit dropdown-item"
                           href="' . route('barangMasukEdit', ['id_barang_masuk' => $id_barang_masuk]) . '">
                            <i class="la la-edit"></i>
                            Edit
                        </a>
                        <a class="akses-detail btn-modal-general dropdown-item"
                           href="#"
                           data-route="' . route('barangMasukDetailModal', ['id_barang_masuk' => $id_barang_masuk]) . '">
                            <i class="la la-info"></i>
                            Detail
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'keywords' => $keywords
        );

        return $json_data;
    }

    function create()
    {
        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Barang Masuk Baru',
                    'route' => ''
                ]
            ]
        );
        $data = Main::data($breadcrumb, $this->menuActive);
        $barang = mBarang::orderBy('brg_kode')->get();


        $data = array_merge(
            $data, array(
                'barang' => $barang,
            )
        );

        return view('barangMasuk/barangMasukCreate', $data);
    }

    function insert(Request $request)
    {
        $rules = [
            'brm_faktur' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'qty_masuk' => 'required',
            'qty_masuk.*' => 'required',
        ];

        $attributes = [
            'id_barang' => 'Barang',
            'qty_masuk' => 'Qty Barang masuk',

        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['qty_masuk.' . $i] = 'Qty Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];

        $id_barang_arr = $request->input('id_barang');

        $brm_faktur = $request->input('brm_faktur');
        $brm_tanggal = Main::format_datetime_db($request->input('tanggal'));
        $brm_keterangan = $request->input('keterangan');

        $bmd_qty_masuk_arr = $request->input('qty_masuk');


        $created_at = date('Y-m-d H:i:s');
        $updated_at = $created_at;


        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke barang masuk data
             */
            $barang_masuk_data = [
                'id_user' => $id_user,
                'brm_faktur' => $brm_faktur,
                'brm_tanggal' => $brm_tanggal,
                'brm_keterangan' => $brm_keterangan,
            ];

            $response_barang_masuk = mBarangMasuk::create($barang_masuk_data);
            $id_barang_masuk = $response_barang_masuk->id_barang_masuk;

            foreach ($id_barang_arr as $index => $id_barang) {

                $bmd_qty_masuk = Main::format_number_db($bmd_qty_masuk_arr[$index]);

                /**
                 * Update Stok Barang
                 */

                $brg_stok_now = mBarang::where('id_barang', $id_barang)->value('brg_stok');
                $brg_stok_after = $brg_stok_now + $bmd_qty_masuk;

                mBarang::where('id_barang', $id_barang)->update(['brg_stok' => $brg_stok_after]);

                /**
                 * Memasukkan data ke barang masuk detail
                 */

                $barang_masuk_detail_data = [
                    'id_barang_masuk' => $id_barang_masuk,
                    'id_barang' => $id_barang,
                    'bmd_qty_masuk' => $bmd_qty_masuk,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at
                ];

                $response_barang_masuk_detail = mBarangMasukDetail::create($barang_masuk_detail_data);
                $id_barang_masuk_detail = $response_barang_masuk_detail->id_barang_masuk_detail;
            }

            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function edit($id_barang_masuk)
    {
        $id_barang_masuk = Main::decrypt($id_barang_masuk);

        $breadcrumb = array_merge(
            $this->breadcrumb,
            [
                [
                    'label' => 'Edit Barang Masuk',
                    'route' => ''
                ]
            ]
        );

        $data = Main::data($breadcrumb, $this->menuActive);
        $barang = mBarang::orderBy('brg_kode')->get();
        $edit = mBarangMasuk
            ::with([
                'barang_masuk_detail',
                'barang_masuk_detail.barang',
            ])
            ->where('id_barang_masuk', $id_barang_masuk)
            ->first();

        $data = array_merge(
            $data, array(
                'barang' => $barang,
                'edit' => $edit
            )
        );

        return view('barangMasuk/barangMasukEdit', $data);
    }

    function update(Request $request, $id_barang_masuk)
    {
        $rules = [
            'brm_faktur' => 'required',
            'id_barang' => 'required',
            'id_barang.*' => 'required',
            'qty_masuk' => 'required',
            'qty_masuk.*' => 'required',
        ];

        $attributes = [
            'id_barang' => 'Barang',
            'qty_masuk' => 'Qty Barang masuk',
        ];

        $attr = [];
        for ($i = 0; $i <= 200; $i++) {
            $next = $i + 1;
            $attr['id_barang.' . $i] = 'Barang ke-' . $next;
            $attr['qty_masuk.' . $i] = 'Qty Barang ke-' . $next;
        }
        $attributes = array_merge($attributes, $attr);
        $validator = Validator::make($request->all(), $rules, [], $attributes);

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()
            ], 422);
        }

        $user = Session::get('user');
        $id_user = $user['id'];
        $created_at = date('Y-m-d H:i:s');
        $updated_at = $created_at;

        $id_barang_masuk = Main::decrypt($id_barang_masuk);
        $id_barang_masuk_detail_arr = $request->input('id_barang_masuk_detail');
        $id_barang_arr = $request->input('id_barang');
        $brm_faktur = $request->input('brm_faktur');

        $brm_tanggal = Main::format_datetime_db($request->input('tanggal'));
        $brm_keterangan = $request->input('keterangan');

        $bmd_qty_masuk_arr = $request->input('qty_masuk');

        $barang_masuk = mBarangMasuk::where('id_barang_masuk', $id_barang_masuk)->first();


        DB::beginTransaction();
        try {

            /**
             * Memasukkan ke barang masuk data
             */
            $barang_masuk_data = [
                'brm_faktur' => $brm_faktur,
                'brm_tanggal' => $brm_tanggal,
                'brm_keterangan' => $brm_keterangan,
            ];

            mBarangMasuk::where('id_barang_masuk', $id_barang_masuk)->update($barang_masuk_data);

            foreach ($id_barang_masuk_detail_arr as $index => $id_barang_masuk_detail) {

                $bmd_qty_masuk = Main::format_number_db($bmd_qty_masuk_arr[$index]);
                $barang_masuk_detail = mBarangMasukDetail::where('id_barang_masuk_detail', $id_barang_masuk_detail)->first();
                $id_barang = $id_barang_arr[$index];

                /**
                 * Update Stok Barang
                 */

                $brg_stok_now = mBarang::where('id_barang', $id_barang)->value('brg_stok');
                $brg_stok_after = ( $brg_stok_now - $barang_masuk_detail->bmd_qty_masuk) + $bmd_qty_masuk;

                mBarang::where('id_barang', $id_barang)->update(['brg_stok' => $brg_stok_after]);

                /**
                 * Memperbarui data ke barang masuk detail
                 */
                $barang_masuk_detail_data = [
                    'bmd_qty_masuk' => $bmd_qty_masuk
                ];

                mBarangMasukDetail::where('id_barang_masuk_detail', $id_barang_masuk_detail)->update($barang_masuk_detail_data);

//                return Response::json(array(
//                    'code'      =>  401,
//                    'message'   =>  [
//                        $brg_stok_now,
//                        $barang_masuk_detail->bmd_qty_masuk,
//                        $bmd_qty_masuk,
//                        $brg_stok_after
//                    ]
//                ), 401);


            }
            DB::commit();
        } catch (Exception $e) {
            throw $e;
            DB::rollBack();
        }
    }

    function detail_modal($id_barang_masuk)
    {
        $id_barang_masuk = Main::decrypt($id_barang_masuk);
        $barang_masuk = mBarangMasuk
            ::with([
                'user',
                'barang_masuk_detail',
                'barang_masuk_detail.barang',
            ])
            ->where('id_barang_masuk', $id_barang_masuk)
            ->first();

        $data = [
            'barang_masuk' => $barang_masuk,
        ];

        return view('barangMasuk.barangMasukDetailModal', $data);
    }
}
