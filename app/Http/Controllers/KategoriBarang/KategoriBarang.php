<?php

namespace app\Http\Controllers\KategoriBarang;

use app\Models\mBarang;
use app\Models\mBarangKategori;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class KategoriBarang extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['kategori_barang'],
                'route' => ''
            ]
        ];
    }

    function index(Request $request)
    {
        $filter_component = Main::date_filter($request, ['keywords']);
        $date_from_db = $filter_component['date_from_db'];
        $date_to_db = $filter_component['date_to_db'];
        $date_filter = $filter_component['date_filter'];
        $keywords = $filter_component['keywords'];


        $data = Main::data($this->breadcrumb);
        $data_list = DB::table('barang_kategori');
        $datatable_column = [
            ["data" => "no"],
            ["data" => "brk_kode"],
            ["data" => "options"],
        ];

        $data = array_merge($data, [
            'data' => $data_list,
            'datatable_column' => $datatable_column,
            'date_filter' => $date_filter,
            'table_data_post' => array(
                'date_from_db' => $date_from_db,
                'date_to_db' => $date_to_db,
                'keywords' => $keywords
            ),
        ]);

        return view('kategoriBarang/kategoriBarangList', $data);
    }


    function data_table(Request $request)
    {

        $data_post = $request->input('data');
        $keywords = $data_post['keywords'];

        if ($keywords) {
            $total_data = mBarangKategori
                ::whereLike('id_barang_kategori', $keywords)
                ->orWhereLike('brk_kode', $keywords)
                ->count();
        } else {
            $total_data = mBarangKategori
                ::count();
        }

        $limit = $request->input('length');
        $start = $request->input('start');
        $order_column = 'id_barang_kategori'; //$columns[$request->input('order.0.column')];
        $order_type = $request->input('order.0.dir');

        if ($keywords) {
            $data_list = mBarangKategori
               ::whereLike('id_barang_kategori', $keywords)
                ->orWhereLike('brk_kode', $keywords)
                ->offset($start)
                ->limit($limit)
                ->orderBy($order_column, $order_type)
                ->get();
        } else {
            $data_list = mBarangKategori
                ::offset($start)
                ->limit($limit)
                ->orderBy($order_column, $order_type)
                ->get();
        }

        $total_data++;

        $data = array();
        foreach ($data_list as $key => $row) {
            $key++;
            $id_barang_kategori = Main::encrypt($row->id_barang_kategori);

            if ($order_type == 'asc') {
                $no = $key + $start;
            } else {
                $no = $total_data - $key - $start;
            }

            $nestedData['no'] = $no;
            $nestedData['brk_kode'] = $row->brk_kode;
            $nestedData['options'] = '
                <div class="dropdown">
                    <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                            type="button"
                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        Menu
                    </button>
                    <div class="dropdown-menu dropdown-menu-right"
                         aria-labelledby="dropdownMenuButton">
                        <a class="akses-action_wait_cancel dropdown-item btn-modal-general"
                           href="#"
                            data-route="' . route('kategoriBarangEditModal', ['id_barang_kategori' => $id_barang_kategori]) . '">
                            <i class="la la-pencil" ></i >
                            Edit
                        </a >

                        <div class="dropdown-divider"></div>
                        <a class="akses-action_wait_detail dropdown-item btn-hapus"
                           href="#"
                            data-route="' . route('kategoriBarangDelete', ['id_barang_kategori' => $id_barang_kategori]) . '">
                            <i class="la la-remove"></i>
                            Hapus
                        </a>
                    </div>
                </div>
            ';


            $data[] = $nestedData;

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($total_data - 1),
            "recordsFiltered" => intval($total_data - 1),
            "data" => $data,
            'all_request' => $request->all(),
            'keywords' => $keywords
        );

        return $json_data;
    }

    function insert(Request $request)
    {
        $request->validate([
            'brk_kode' => 'required',
        ]);

        $data = $request->except('_token');
        mBarangKategori::create($data);
    }

    function edit_modal($id_barang_kategori)
    {
        $id_barang_kategori = Main::decrypt($id_barang_kategori);
        $edit = mBarangKategori::where('id_barang_kategori', $id_barang_kategori)->first();
        $data = [
            'edit' => $edit,
        ];

        return view('kategoriBarang/kategoriBarangEditModal', $data);
    }

    function delete($id_barang_kategori)
    {
        $id_barang_kategori = Main::decrypt($id_barang_kategori);
        mBarangKategori::where('id_barang_kategori', $id_barang_kategori)->delete();
    }

    function update(Request $request, $id_barang_kategori)
    {
        $id_barang_kategori = Main::decrypt($id_barang_kategori);
        $request->validate([
            'brk_kode' => 'required',
        ]);
        $data = $request->except("_token");
        mBarangKategori::where(['id_barang_kategori' => $id_barang_kategori])->update($data);
    }
}
