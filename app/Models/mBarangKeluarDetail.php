<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mBarangKeluarDetail extends Model
{
    use SoftDeletes;

    protected $table = 'barang_keluar_detail';
    protected $primaryKey = 'id_barang_keluar_detail';
    protected $fillable = [
        'id_barang_keluar',
        'id_barang',
        'bkd_qty_keluar',

    ];

    public function barang_keluar() {
        return $this->belongsTo(mBarangKeluar::class, 'id_barang_keluar');
    }

    public function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'LIKE', '%' . $value . '%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'LIKE', '%' . $value . '%');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
