<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mBarangKeluar extends Model
{
    use SoftDeletes;

    protected $table = 'barang_keluar';
    protected $primaryKey = 'id_barang_keluar';
    protected $fillable = [
        'id_user',
        'brk_faktur',
        'brk_tanggal',
        'brk_keterangan',
        'brk_status',
        'brk_tipe',
        'brk_penanggung_jawab'
    ];

    public function user()
    {
        return $this->belongsTo(mUser::class, 'id_user');
    }

    public function barang_keluar_detail()
    {
        return $this->hasMany(mBarangKeluarDetail::class, 'id_barang_keluar');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'LIKE', '%' . $value . '%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'LIKE', '%' . $value . '%');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
