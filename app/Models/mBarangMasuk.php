<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mBarangMasuk extends Model
{
    use SoftDeletes;

    protected $table = 'barang_masuk';
    protected $primaryKey = 'id_barang_masuk';
    protected $fillable = [
        'id_user',
        'brm_faktur',
        'brm_tanggal',
        'brm_keterangan',
        'brm_penanggung_jawab',
    ];

    public function user()
    {
        return $this->belongsTo(mUser::class, 'id_user');
    }

    public function barang_masuk_detail()
    {
        return $this->hasMany(mBarangMasukDetail::class, 'id_barang_masuk');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'LIKE', '%' . $value . '%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'LIKE', '%' . $value . '%');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
