<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mBarangMasukDetail extends Model
{
    use SoftDeletes;

    protected $table = 'barang_masuk_detail';
    protected $primaryKey = 'id_barang_masuk_detail';
    protected $fillable = [
        'id_barang_masuk',
        'id_barang',
        'bmd_qty_masuk',

    ];

    public function barang_masuk() {
        return $this->belongsTo(mBarangMasuk::class, 'id_barang_masuk');
    }

    public function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'LIKE', '%' . $value . '%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'LIKE', '%' . $value . '%');
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
