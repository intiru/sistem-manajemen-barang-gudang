-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 17, 2022 at 02:19 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intiru_sistem_management_gudang`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `id_barang_kategori` int(11) DEFAULT NULL,
  `brg_kode` varchar(255) DEFAULT NULL,
  `brg_nama` varchar(255) DEFAULT NULL,
  `brg_stok` int(11) DEFAULT NULL,
  `brg_satuan` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `id_barang_kategori`, `brg_kode`, `brg_nama`, `brg_stok`, `brg_satuan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'ABC', 'Baju', 10, '1', '2022-04-04 05:27:36', '2022-04-04 05:27:36', '2022-04-04 05:27:36'),
(2, 2, 'DEF', 'Sepatu', 5, '2', '2022-04-04 05:27:36', '2022-04-04 05:27:36', '2022-04-04 05:27:36'),
(3, 1, 'hvs001', 'Hvs 100 Gsm A3+ (32x48)', 10, 'Rim', '2022-04-04 13:10:37', '2022-07-12 22:30:02', NULL),
(4, 1, 'Hv002', 'Hvs 100 Gsm A4', 5, 'Rim', '2022-04-05 12:30:13', '2022-07-12 22:29:45', NULL),
(5, 1, '1234567', 'Tasgf', 24, '5', '2022-04-05 12:32:21', '2022-04-05 12:32:53', '2022-04-05 12:32:53'),
(6, 1, 'Hv003', 'Hvs 100 Gsm F4', 5, 'Rim', '2022-04-07 15:48:04', '2022-07-12 22:30:45', NULL),
(7, 1, 'Hv004', 'Hvs 80 Gsm 36x48', 20, 'Rim', '2022-04-12 13:13:12', '2022-07-14 09:49:59', '2022-07-14 09:49:59'),
(8, 1, 'Hv005', 'Hvs 80gr Paper 60x90', 10, 'Rim', '2022-04-12 13:13:58', '2022-07-12 22:32:30', NULL),
(9, 2, 'djfnsoljnv666788', 'njdknefgttttttttt', 90, '9', '2022-04-13 13:49:37', '2022-04-13 13:50:19', '2022-04-13 13:50:19'),
(10, 2, '11', '21', 31, '41', '2022-07-12 08:50:32', '2022-07-12 08:50:52', '2022-07-12 08:50:52'),
(11, 2, 'BF001', 'Bufalo Pindodeli A3 Abu-abu', 20, 'Lembar', '2022-07-14 09:54:30', '2022-07-14 09:54:30', NULL),
(12, 2, 'BF002', 'Bufalo Pindodeli A3 Merah', 10, 'Lembar', '2022-07-14 09:55:24', '2022-07-14 09:55:24', NULL),
(13, 2, 'BF003', 'Bufalo Pindodeli A3 Putih', 30, 'Lembar', '2022-07-14 09:56:01', '2022-07-14 09:56:01', NULL),
(14, 5, 'MN001', 'Manila Bc Putih Plus 33x48', 15, 'Lembar', '2022-07-14 09:56:48', '2022-07-14 09:56:48', NULL),
(15, 5, 'MN009', 'Manila Pindodeli A4 Putih', 22, 'Lembarr', '2022-07-14 09:57:32', '2022-07-14 09:57:32', NULL),
(16, 8, 'AP004', 'Art Paper 210 Gsm 33x65', 35, 'Lembar', '2022-07-14 09:58:27', '2022-07-14 09:58:27', NULL),
(17, 8, 'AP008', 'Art Paper 260 Gsm 39x54 (isi 250)', 4, 'Pack', '2022-07-14 10:00:00', '2022-07-14 10:00:00', NULL),
(18, 9, 'INK001', 'Ink Epson Black 1 Liter', 8, 'Botol', '2022-07-14 10:01:52', '2022-07-14 10:01:52', NULL),
(19, 9, 'INK002', 'Ink Epson Cyan 1 Liter', 2, 'Botol', '2022-07-14 10:02:29', '2022-07-14 10:02:29', NULL),
(20, 11, 'KT008', 'Karton Folio', 20, 'Lembar', '2022-07-14 10:03:10', '2022-07-15 09:15:26', NULL),
(21, 11, 'KT007', 'Karton A5', 21, 'Lembar', '2022-07-14 10:03:32', '2022-07-14 10:03:32', NULL),
(22, 12, 'KWT001', 'Kawat Spiral 1/2', 25, 'Lonjor', '2022-07-14 10:04:29', '2022-07-14 10:04:29', NULL),
(23, 12, 'KWT002', 'Kawat Spiral 1/4', 12, 'Lonjor', '2022-07-14 10:05:00', '2022-07-14 10:05:00', NULL),
(24, 12, 'KWT007', 'Kawat Spiral 5/16', 20, 'Lonjor', '2022-07-14 10:05:34', '2022-07-14 10:05:34', NULL),
(25, 15, 'LMT001', 'Laminating Film Glosy 140gram 42\'\'x46 M', 3, 'Roll', '2022-07-14 10:06:48', '2022-07-14 10:06:48', NULL),
(26, 15, 'LMT002', 'Laminating Film Glosy 100 Gr 50\'\'x46 M', 10, 'Roll', '2022-07-14 10:08:08', '2022-07-14 10:08:08', NULL),
(27, 16, 'LEM001', 'Lem Acrylic', 1, 'Botol', '2022-07-14 10:09:41', '2022-07-14 10:10:01', NULL),
(28, 16, 'Lem002', 'Lem Fox Putih', 2, 'Kilo', '2022-07-14 10:10:36', '2022-07-14 10:10:36', NULL),
(29, 17, 'PLK011', 'Plastik Wrapping 50cm', 3, 'Roll', '2022-07-14 10:14:46', '2022-07-14 10:14:46', NULL),
(30, 17, 'PLK004', 'Plastik Laminating Thermal Dop', 5, 'Roll', '2022-07-14 10:16:18', '2022-07-14 10:16:18', NULL),
(31, 17, 'PLK005', 'Plastik Laminating Thermal Gloss 35cm', 3, 'Roll', '2022-07-14 10:17:11', '2022-07-14 10:17:11', NULL),
(32, 19, 'TNT003', 'Tinta Soven Black 5L', 1, 'Jerigen', '2022-07-14 10:17:53', '2022-07-14 10:17:53', NULL),
(33, 19, 'TNT011', 'Tinta Sublim Magenta', 10, 'Botol', '2022-07-14 10:18:28', '2022-07-14 10:18:28', NULL),
(34, 20, 'MP001', 'Map Folio', 25, 'PCS', '2022-07-14 10:19:03', '2022-07-15 09:15:10', NULL),
(35, 20, 'MP004', 'Map Folder', 10, 'PCS', '2022-07-14 10:19:26', '2022-07-14 10:19:26', NULL),
(36, 20, 'MP005', 'Map Odner', 15, 'PCS', '2022-07-14 10:19:55', '2022-07-14 10:19:55', NULL),
(37, 20, 'MP006', 'Map Kantong', 20, 'Pcs', '2022-07-14 10:20:33', '2022-07-14 10:20:33', NULL),
(38, 21, 'AMP001', 'Amplop CD', 5, 'Box', '2022-07-14 10:21:03', '2022-07-14 10:21:03', NULL),
(39, 21, 'AMP002', 'Amplop Window', 3, 'Box', '2022-07-14 10:21:23', '2022-07-14 10:21:23', NULL),
(40, 21, 'AMP003', 'Amplop Kontak', 2, 'Box', '2022-07-14 10:21:40', '2022-07-14 10:21:40', NULL),
(41, 21, 'AMP005', 'Amplop Pendek', 6, 'Box', '2022-07-14 10:22:10', '2022-07-14 10:28:48', NULL),
(42, 21, 'AMP006', 'Amplop Panjang', 3, 'Box', '2022-07-14 10:22:31', '2022-07-14 10:22:31', NULL),
(43, 27, 'BK001', 'Buku Tulis \"38\" Sinar Dunia', 15, 'Pcs', '2022-07-14 10:24:15', '2022-07-15 09:14:32', NULL),
(44, 27, 'BK003', 'Buku Gambar A3 SIDU', 11, 'Pcs', '2022-07-14 10:25:04', '2022-07-14 10:25:04', NULL),
(45, 27, 'BK004', 'Buku Gambar A4 SIDU', 19, 'Pcs', '2022-07-14 10:25:32', '2022-07-14 10:25:32', NULL),
(46, 27, 'BK005', 'Buku Folio Ria', 8, 'Pcs', '2022-07-14 10:26:05', '2022-07-14 10:26:05', NULL),
(47, 28, 'PS001', 'Pensil 2B Faber Castel', 2, 'Lusin', '2022-07-14 10:27:01', '2022-07-14 10:27:01', NULL),
(48, 28, 'PS002', 'Pensil 2B Joyko', 1, 'Lusin', '2022-07-14 10:27:27', '2022-07-14 10:27:27', NULL),
(49, 28, 'PS006', 'Pensil 4B Kenko', 1, 'Lusin', '2022-07-14 10:28:10', '2022-07-14 10:28:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `barang_kategori`
--

CREATE TABLE `barang_kategori` (
  `id_barang_kategori` int(11) NOT NULL,
  `brk_kode` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_kategori`
--

INSERT INTO `barang_kategori` (`id_barang_kategori`, `brk_kode`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hvs', '2022-04-04 05:56:59', '2022-07-12 22:08:51', NULL),
(2, 'Bufalo', '2022-04-04 05:56:59', '2022-07-12 22:08:40', NULL),
(3, 'BA', '2022-04-05 13:26:43', '2022-04-05 13:27:05', '2022-04-05 13:27:05'),
(4, 'C', '2022-04-13 14:12:40', '2022-04-13 14:13:02', '2022-04-13 14:13:02'),
(5, 'Manila', '2022-04-13 14:13:17', '2022-07-12 22:08:27', NULL),
(6, 'ABCD', '2022-04-14 11:28:03', '2022-04-14 11:50:16', '2022-04-14 11:50:16'),
(7, 'Stample', '2022-07-12 22:08:59', '2022-07-12 22:08:59', NULL),
(8, 'Art Paper', '2022-07-12 22:09:11', '2022-07-12 22:09:11', NULL),
(9, 'Ink', '2022-07-12 22:09:18', '2022-07-12 22:09:18', NULL),
(10, 'Ivoy', '2022-07-12 22:09:27', '2022-07-12 22:09:27', NULL),
(11, 'Karton', '2022-07-12 22:09:39', '2022-07-12 22:09:39', NULL),
(12, 'Kawat', '2022-07-12 22:09:46', '2022-07-12 22:09:46', NULL),
(13, 'Kraft', '2022-07-12 22:09:53', '2022-07-12 22:09:53', NULL),
(14, 'Laminasi', '2022-07-12 22:10:02', '2022-07-12 22:10:02', NULL),
(15, 'Lamiinating', '2022-07-12 22:10:13', '2022-07-12 22:10:13', NULL),
(16, 'Lem', '2022-07-12 22:10:19', '2022-07-12 22:10:19', NULL),
(17, 'Plastik', '2022-07-12 22:10:27', '2022-07-12 22:10:27', NULL),
(18, 'Stiker', '2022-07-12 22:10:34', '2022-07-12 22:10:34', NULL),
(19, 'Tinta', '2022-07-12 22:10:47', '2022-07-12 22:10:47', NULL),
(20, 'Map', '2022-07-12 22:24:01', '2022-07-12 22:24:01', NULL),
(21, 'Amplop', '2022-07-12 22:24:19', '2022-07-12 22:24:19', NULL),
(22, 'Komputer/Pc', '2022-07-12 22:24:30', '2022-07-12 22:24:30', NULL),
(23, 'Laptop', '2022-07-12 22:24:38', '2022-07-12 22:24:38', NULL),
(24, 'Scanner', '2022-07-12 22:24:47', '2022-07-12 22:24:47', NULL),
(25, 'Printer', '2022-07-12 22:25:00', '2022-07-12 22:25:00', NULL),
(26, 'Camera', '2022-07-12 22:25:08', '2022-07-12 22:25:08', NULL),
(27, 'Buku', '2022-07-12 22:25:18', '2022-07-12 22:25:18', NULL),
(28, 'Pensil', '2022-07-12 22:25:25', '2022-07-12 22:25:25', NULL),
(29, 'Pulpen', '2022-07-12 22:25:31', '2022-07-12 22:25:31', NULL),
(30, 'Binder', '2022-07-12 22:25:38', '2022-07-12 22:25:38', NULL),
(31, 'Penghapus', '2022-07-12 22:25:51', '2022-07-12 22:25:51', NULL),
(32, 'Penggaris', '2022-07-12 22:25:59', '2022-07-12 22:25:59', NULL),
(33, 'Tipe-X', '2022-07-12 22:26:08', '2022-07-12 22:26:08', NULL),
(34, 'Paper Clip', '2022-07-12 22:26:18', '2022-07-12 22:26:18', NULL),
(35, 'Gunting', '2022-07-12 22:26:27', '2022-07-12 22:26:27', NULL),
(36, 'Cutter', '2022-07-12 22:26:38', '2022-07-12 22:26:38', NULL),
(37, 'Staples', '2022-07-12 22:26:47', '2022-07-12 22:26:47', NULL),
(38, 'Lakban', '2022-07-12 22:26:57', '2022-07-12 22:26:57', NULL),
(39, 'Frame Foto', '2022-07-12 22:27:07', '2022-07-12 22:27:07', NULL),
(40, 'Foto Album', '2022-07-12 22:27:15', '2022-07-12 22:27:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `id_barang_keluar` int(11) NOT NULL,
  `brk_faktur` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `brk_tanggal` datetime DEFAULT NULL,
  `brk_keterangan` text DEFAULT NULL,
  `brk_status` enum('pengajuan','terima','tolak') DEFAULT NULL,
  `brk_tipe` varchar(255) DEFAULT NULL,
  `brk_penanggung_jawab` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_keluar`
--

INSERT INTO `barang_keluar` (`id_barang_keluar`, `brk_faktur`, `id_user`, `brk_tanggal`, `brk_keterangan`, `brk_status`, `brk_tipe`, `brk_penanggung_jawab`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 546, 1, '2022-04-08 09:17:16', 'sgvc', 'pengajuan', NULL, NULL, '2022-04-08 09:17:16', '2022-04-08 09:17:16', '2022-04-08 09:17:16'),
(2, 0, 23, '2022-04-11 14:46:00', 'dfzgfdh', 'pengajuan', NULL, NULL, '2022-04-11 14:46:49', '2022-04-11 14:46:49', NULL),
(3, 0, 23, '2022-04-11 14:52:00', 'efvge', 'pengajuan', NULL, NULL, '2022-04-11 14:52:41', '2022-04-11 14:52:41', NULL),
(4, 12345, 23, '2022-04-12 10:35:00', 'ewfbdfe', NULL, NULL, NULL, '2022-04-12 10:36:23', '2022-04-12 10:36:23', NULL),
(5, 12345, 23, '2022-04-12 10:55:00', 'wfwgwe', 'pengajuan', NULL, NULL, '2022-04-12 10:56:07', '2022-04-13 11:25:00', NULL),
(6, 9898780, 23, '2022-04-12 10:56:00', 'dvcefv', 'tolak', NULL, NULL, '2022-04-12 10:56:55', '2022-04-13 11:24:23', NULL),
(7, 98765, 23, '2022-04-12 12:53:00', 'zxcvbnm', 'pengajuan', NULL, NULL, '2022-04-12 12:53:43', '2022-04-12 12:53:43', NULL),
(8, 123456789, 23, '2022-04-12 13:37:00', 'hjkloeffe', 'terima', NULL, NULL, '2022-04-12 13:38:07', '2022-04-13 11:23:47', NULL),
(9, 345778899, 36, '2022-04-13 11:28:00', 'qweribeidfbikwflosdkmcxishBCdsibUdxbh', 'pengajuan', NULL, NULL, '2022-04-13 11:29:12', '2022-04-13 11:31:01', NULL),
(10, 345, 37, '2022-04-13 14:31:00', 'tolong di konfirmasi', 'pengajuan', NULL, NULL, '2022-04-13 14:32:24', '2022-04-13 14:32:24', NULL),
(11, 123, 37, '2022-04-14 11:56:00', '12313', 'terima', NULL, NULL, '2022-04-14 11:57:09', '2022-04-14 12:06:13', NULL),
(12, 123456, 37, '2022-04-14 12:20:00', 'abcdefghijklmn', 'terima', NULL, NULL, '2022-04-14 12:22:16', '2022-04-14 15:01:27', NULL),
(13, 55678, 37, '2022-04-14 13:30:00', 'klmopqrstu', 'tolak', NULL, NULL, '2022-04-14 13:30:51', '2022-04-14 14:38:51', NULL),
(14, 667789, 37, '2022-04-14 15:03:00', 'barang dari jakarta', 'pengajuan', NULL, NULL, '2022-04-14 15:04:41', '2022-04-14 15:04:41', NULL),
(15, 1, 37, '2022-04-14 15:08:00', '2', 'terima', NULL, NULL, '2022-04-14 15:08:30', '2022-04-14 15:09:08', NULL),
(16, 2, 36, '2022-07-12 09:00:00', '3', 'pengajuan', NULL, '123123', '2022-07-12 09:04:20', '2022-07-12 09:25:09', NULL),
(17, 123, 36, '2022-07-12 09:23:00', '123', 'pengajuan', NULL, '123123123111111', '2022-07-12 09:23:27', '2022-07-12 09:29:55', NULL),
(18, 1101, 36, '2022-07-12 22:33:00', 'kehabisan', 'terima', NULL, 'Kepala Gudang', '2022-07-12 22:34:27', '2022-07-12 22:35:07', NULL),
(19, 1103, 36, '2022-07-14 10:36:00', 'stok barang mau habis', 'terima', NULL, 'Kadek Ulandari', '2022-07-14 10:39:43', '2022-07-14 10:41:42', NULL),
(20, 1101, 36, '2022-07-15 09:16:00', 'Barang Habis', 'pengajuan', NULL, 'Staf foto copy', '2022-07-15 09:17:59', '2022-07-15 09:18:35', NULL),
(21, 1103, 36, '2022-07-15 09:18:00', 'Habis', 'terima', NULL, 'Staf foto copy', '2022-07-15 09:19:28', '2022-07-15 09:19:55', NULL),
(22, 1104, 36, '2022-07-15 09:23:00', 'Stock dikit', 'terima', NULL, 'Staf foto copy', '2022-07-15 09:24:16', '2022-07-15 09:24:50', NULL),
(23, 1105, 36, '2022-07-15 10:11:00', 'Stock Habis', 'terima', NULL, 'Staf foto copy', '2022-07-15 10:12:12', '2022-07-15 10:12:51', NULL),
(24, 2, 36, '2022-08-07 16:32:00', '3', 'pengajuan', 'barang_rusak', '1', '2022-08-07 16:32:58', '2022-08-07 16:34:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar_detail`
--

CREATE TABLE `barang_keluar_detail` (
  `id_barang_keluar_detail` int(11) NOT NULL,
  `id_barang_keluar` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `bkd_qty_keluar` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_keluar_detail`
--

INSERT INTO `barang_keluar_detail` (`id_barang_keluar_detail`, `id_barang_keluar`, `id_barang`, `bkd_qty_keluar`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 4, 100, '2022-04-11 14:46:49', '2022-04-11 14:46:49', NULL),
(2, 3, 6, 600, '2022-04-11 14:52:41', '2022-04-11 14:52:41', NULL),
(3, 4, 4, 900, '2022-04-12 10:36:23', '2022-04-12 10:36:23', NULL),
(4, 5, 4, 50, '2022-04-12 10:56:07', '2022-04-13 11:25:00', NULL),
(5, 6, 6, 90, '2022-04-12 10:56:56', '2022-04-13 11:24:23', NULL),
(6, 7, 6, 97, '2022-04-12 12:53:43', '2022-04-12 12:53:43', NULL),
(7, 8, 8, 30, '2022-04-12 13:38:07', '2022-04-13 11:23:47', NULL),
(8, 8, 7, 25, '2022-04-12 13:38:07', '2022-04-13 11:23:47', NULL),
(9, 9, 6, 90, '2022-04-13 11:29:14', '2022-04-13 11:31:01', NULL),
(10, 9, 7, 87, '2022-04-13 11:29:14', '2022-04-13 11:31:01', NULL),
(11, 10, 4, 10, '2022-04-13 14:32:26', '2022-04-13 14:32:26', NULL),
(12, 10, 6, 20, '2022-04-13 14:32:26', '2022-04-13 14:32:26', NULL),
(13, 10, 7, 40, '2022-04-13 14:32:26', '2022-04-13 14:32:26', NULL),
(14, 10, 8, 45, '2022-04-13 14:32:26', '2022-04-13 14:32:26', NULL),
(15, 11, 4, 1, '2022-04-14 11:57:09', '2022-04-14 11:57:09', NULL),
(16, 12, 4, 60, '2022-04-14 12:22:18', '2022-04-14 12:42:39', NULL),
(17, 12, 6, 20, '2022-04-14 12:22:18', '2022-04-14 12:42:39', NULL),
(18, 13, 4, 5, '2022-04-14 13:30:52', '2022-04-14 13:30:52', NULL),
(19, 13, 8, 10, '2022-04-14 13:30:52', '2022-04-14 13:30:52', NULL),
(20, 14, 7, 100, '2022-04-14 15:04:43', '2022-04-14 15:04:43', NULL),
(21, 14, 8, 80, '2022-04-14 15:04:43', '2022-04-14 15:04:43', NULL),
(22, 15, 3, 4, '2022-04-14 15:08:30', '2022-04-14 15:08:30', NULL),
(23, 16, 7, 1, '2022-07-12 09:04:20', '2022-07-12 09:25:09', NULL),
(24, 17, 8, 2, '2022-07-12 09:23:27', '2022-07-12 09:29:55', NULL),
(25, 18, 3, 10, '2022-07-12 22:34:27', '2022-07-12 22:34:27', NULL),
(26, 19, 49, 1, '2022-07-14 10:39:43', '2022-07-14 10:39:43', NULL),
(27, 20, 6, 1, '2022-07-15 09:17:59', '2022-07-15 09:18:35', NULL),
(28, 21, 8, 5, '2022-07-15 09:19:28', '2022-07-15 09:19:28', NULL),
(29, 22, 8, 1, '2022-07-15 09:24:16', '2022-07-15 09:24:16', NULL),
(30, 23, 8, 5, '2022-07-15 10:12:13', '2022-07-15 10:12:13', NULL),
(31, 24, 39, 1, '2022-08-07 16:32:58', '2022-08-07 16:34:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id_barang_masuk` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `brm_faktur` varchar(50) DEFAULT NULL,
  `brm_tanggal` datetime DEFAULT NULL,
  `brm_keterangan` text DEFAULT NULL,
  `brm_penanggung_jawab` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id_barang_masuk`, `id_user`, `brm_faktur`, `brm_tanggal`, `brm_keterangan`, `brm_penanggung_jawab`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 23, '092', '2022-04-07 14:17:00', '123', NULL, '2022-04-07 14:18:21', '2022-04-07 14:18:21', NULL),
(3, 23, NULL, '2022-04-07 14:35:00', '345', NULL, '2022-04-07 14:35:39', '2022-04-07 14:35:39', NULL),
(4, NULL, NULL, NULL, NULL, NULL, '2022-04-07 14:35:40', '2022-04-07 14:35:40', NULL),
(5, 23, NULL, '2022-04-07 14:55:00', '456', NULL, '2022-04-07 14:56:17', '2022-04-07 14:56:17', NULL),
(6, NULL, NULL, NULL, NULL, NULL, '2022-04-07 14:56:17', '2022-04-07 14:56:17', NULL),
(7, 23, NULL, '2022-04-08 11:59:00', '4', NULL, '2022-04-08 11:59:47', '2022-04-08 11:59:47', NULL),
(8, NULL, NULL, NULL, NULL, NULL, '2022-04-08 11:59:47', '2022-04-08 11:59:47', NULL),
(9, 23, NULL, '2022-04-08 12:22:00', '4', NULL, '2022-04-08 12:23:28', '2022-04-08 12:23:28', NULL),
(10, NULL, NULL, NULL, NULL, NULL, '2022-04-08 12:23:28', '2022-04-08 12:23:28', NULL),
(11, 23, '123', '2022-04-08 12:28:00', '4', NULL, '2022-04-08 12:29:25', '2022-04-08 12:29:25', NULL),
(12, NULL, NULL, NULL, NULL, NULL, '2022-04-08 12:29:26', '2022-04-08 12:29:26', NULL),
(13, NULL, NULL, NULL, NULL, NULL, '2022-04-08 12:29:26', '2022-04-08 12:29:26', NULL),
(14, 23, '567', '2022-04-08 13:05:00', '4', NULL, '2022-04-08 13:05:42', '2022-04-08 13:05:42', NULL),
(15, NULL, NULL, NULL, NULL, NULL, '2022-04-08 13:05:44', '2022-04-08 13:05:44', NULL),
(16, 23, '789', '2022-04-08 13:11:00', '4', NULL, '2022-04-08 13:12:23', '2022-04-08 13:12:23', NULL),
(17, NULL, NULL, NULL, NULL, NULL, '2022-04-08 13:12:23', '2022-04-08 13:12:23', NULL),
(18, 23, '567', '2022-04-08 13:16:00', '4', NULL, '2022-04-08 13:16:48', '2022-04-08 13:16:48', NULL),
(19, NULL, NULL, NULL, NULL, NULL, '2022-04-08 13:16:48', '2022-04-08 13:16:48', NULL),
(20, 23, '123', '2022-04-08 13:26:00', '4', NULL, '2022-04-08 13:26:48', '2022-04-08 13:26:48', NULL),
(21, NULL, NULL, NULL, NULL, NULL, '2022-04-08 13:26:48', '2022-04-08 13:26:48', NULL),
(22, 23, '657', '2022-04-08 16:26:00', 'fkjbk', NULL, '2022-04-08 16:27:17', '2022-04-08 16:27:17', NULL),
(23, NULL, NULL, NULL, NULL, NULL, '2022-04-08 16:27:17', '2022-04-08 16:27:17', NULL),
(24, 23, '567', '2022-04-08 16:29:00', 'gbgfxh', NULL, '2022-04-08 16:29:35', '2022-04-08 16:29:35', NULL),
(25, NULL, NULL, NULL, NULL, NULL, '2022-04-08 16:29:35', '2022-04-08 16:29:35', NULL),
(26, 23, '4567', '2022-04-08 16:37:00', 'fkvdk jbzk', NULL, '2022-04-08 16:38:08', '2022-04-08 16:38:08', NULL),
(27, 23, '9870', '2022-04-12 11:39:00', 'dvss', NULL, '2022-04-12 11:40:22', '2022-04-12 11:40:22', NULL),
(28, 23, '876', '2022-04-12 13:10:00', 'hjklm', NULL, '2022-04-12 13:11:21', '2022-04-12 13:11:21', NULL),
(29, 35, '456', '2022-04-12 14:33:00', 'tyukmlwm', NULL, '2022-04-12 14:34:57', '2022-04-12 14:34:57', NULL),
(30, 35, '7887498765', '2022-04-13 11:38:00', 'ljdsfkwieufpkasnckjdlshfsnlnvjsdvlsndcb n', NULL, '2022-04-13 11:38:40', '2022-04-13 11:40:49', NULL),
(31, 35, '57768690', '2022-04-13 12:55:00', 'abcdefg', NULL, '2022-04-13 12:55:44', '2022-04-13 12:55:44', NULL),
(32, 37, '234', '2022-04-13 14:28:00', 'ghjytklm', NULL, '2022-04-13 14:28:51', '2022-04-13 14:28:51', NULL),
(33, 37, '8890', '2022-04-14 11:22:00', 'Barang pecah belah', NULL, '2022-04-14 11:23:22', '2022-04-14 11:23:22', NULL),
(34, 35, '456/klm/bli', '2022-04-14 14:28:00', 'Barang dari bali', NULL, '2022-04-14 14:28:51', '2022-04-14 14:28:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk_detail`
--

CREATE TABLE `barang_masuk_detail` (
  `id_barang_masuk_detail` int(11) NOT NULL,
  `id_barang_masuk` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `bmd_qty_masuk` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang_masuk_detail`
--

INSERT INTO `barang_masuk_detail` (`id_barang_masuk_detail`, `id_barang_masuk`, `id_barang`, `bmd_qty_masuk`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 26, 4, 500, '2022-04-08 16:38:08', '2022-04-08 16:38:08', NULL),
(2, 27, 6, 9090, '2022-04-12 11:40:22', '2022-04-12 11:40:22', NULL),
(3, 28, 6, 100, '2022-04-12 13:11:21', '2022-04-12 13:11:21', NULL),
(4, 29, 4, 70, '2022-04-12 14:34:57', '2022-04-12 14:34:57', NULL),
(5, 29, 6, 60, '2022-04-12 14:34:57', '2022-04-12 14:34:57', NULL),
(6, 29, 7, 50, '2022-04-12 14:34:57', '2022-04-12 14:34:57', NULL),
(7, 29, 8, 40, '2022-04-12 14:34:57', '2022-04-12 14:34:57', NULL),
(8, 30, 6, 809, '2022-04-13 11:38:40', '2022-04-13 11:40:50', NULL),
(9, 31, 7, 50, '2022-04-13 12:55:45', '2022-04-13 12:55:45', NULL),
(10, 31, 8, 40, '2022-04-13 12:55:45', '2022-04-13 12:55:45', NULL),
(11, 31, 4, 30, '2022-04-13 12:55:45', '2022-04-13 12:55:45', NULL),
(12, 32, 6, 80, '2022-04-13 14:28:51', '2022-04-13 14:28:51', NULL),
(13, 32, 7, 90, '2022-04-13 14:28:51', '2022-04-13 14:28:51', NULL),
(14, 33, 7, 50, '2022-04-14 11:23:22', '2022-04-14 11:23:22', NULL),
(15, 33, 8, 45, '2022-04-14 11:23:22', '2022-04-14 11:23:22', NULL),
(16, 34, 7, 60, '2022-04-14 14:28:51', '2022-04-14 14:28:51', NULL),
(17, 34, 8, 70, '2022-04-14 14:28:51', '2022-04-14 14:28:51', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `id_karyawan` int(11) DEFAULT NULL,
  `id_user_role` int(11) DEFAULT NULL,
  `nama_karyawan` varchar(255) DEFAULT NULL,
  `alamat_karyawan` varchar(255) DEFAULT NULL,
  `email_karyawan` varchar(255) DEFAULT NULL,
  `foto_karyawan` varchar(255) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `inisial_karyawan` varchar(10) DEFAULT NULL,
  `id_lokasi` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `id_karyawan`, `id_user_role`, `nama_karyawan`, `alamat_karyawan`, `email_karyawan`, `foto_karyawan`, `username`, `password`, `inisial_karyawan`, `id_lokasi`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 2, NULL, NULL, NULL, NULL, 'mahendrawardana', '$2y$10$3/dnn5l4R2C1BKnAOSThw.DA14SiQPcO1Vuh.JsUSzfxyfm.iNxoy', 'MD', '3', '2019-02-08 08:20:40', '2020-12-05 12:14:24', '2020-12-05 12:14:24'),
(24, 5, 16, NULL, NULL, NULL, NULL, 'foreman', '$2y$10$8NNuiOLYUksBUoK7ScaP/.F11uxV/AWC5I.JAdlLrj0kLtUqTv9I6', 'foreman', '6', '2020-08-02 18:19:05', '2020-12-05 12:15:04', '2020-12-05 12:15:04'),
(25, 5, 15, NULL, NULL, NULL, NULL, 'sa', '$2y$10$GoC/aVcCeMdh0KuocdJLue6HKD0kXjkWvKW8Tt7aKltYuj9Q/bsIe', 'sa', '6', '2020-08-02 18:19:22', '2020-12-05 12:14:16', '2020-12-05 12:14:16'),
(26, 5, 2, 'Rista Septiana Rakhmawati', 'Nusa Dua', 'ristasptn13@gmail.com', 'download.jpg', 'admin', '$2y$10$fRv9NHoIVStwvd3aHWzFd.C2sWaCw60kxJbEdgymq2yxspRp6yIl.', 'admin', '6', '2020-08-15 15:37:25', '2022-07-14 10:32:27', NULL),
(27, 5, 19, NULL, NULL, NULL, NULL, 'perawat', '$2y$10$Tj/zmxDfSx3Ps.PzHY4rguO.1gQUjNfl0zkbLjkjaMMk4In2jqeB2', NULL, NULL, '2020-12-05 12:27:06', '2020-12-07 02:48:37', '2020-12-07 02:48:37'),
(28, 124, 19, NULL, NULL, NULL, NULL, 'dwi', '$2y$10$eC6xi0sk.47Xb9fCqcCS6uvR58ADVMcb4y61X2IbRXUF46fVJ3Amy', NULL, NULL, '2020-12-07 02:49:00', '2021-06-18 09:56:42', '2021-06-18 09:56:42'),
(29, 125, 19, NULL, NULL, NULL, NULL, 'adi', '$2y$10$8VF6pw.B7fdV1SYaopXwPO.k4IQ6xZGRETE00vKlORzEX96.lzPe2', NULL, NULL, '2020-12-07 02:49:16', '2021-06-18 09:56:33', '2021-06-18 09:56:33'),
(30, 121, 18, NULL, NULL, NULL, NULL, 'ary', '$2y$10$2WK0Lgc3QGbdsAatWvldde/3CgmVBuW5kC.qnrJIe6n1aaWWLEm8C', NULL, NULL, '2020-12-07 02:49:33', '2021-06-18 09:56:37', '2021-06-18 09:56:37'),
(31, 123, 19, NULL, NULL, NULL, NULL, 'komang', '$2y$10$FiRvFwFUJQDSbEh5v8msSORkoFhBmeykttQsUSgKa7y0VU7RVgfBa', NULL, NULL, '2020-12-07 02:50:34', '2021-06-18 09:56:46', '2021-06-18 09:56:46'),
(32, 122, 2, NULL, NULL, NULL, NULL, 'mitha', '$2y$10$F5NkwFOT2TiGeeQcHWyZiuQeCvf0rR3iK2jff885BykKo3Eses/yW', NULL, NULL, '2020-12-07 02:50:53', '2021-06-18 09:56:27', '2021-06-18 09:56:27'),
(33, 5, 20, NULL, NULL, NULL, NULL, 'wina', '$2y$10$cRwbXhhDumMiqtrNmGVhV.sgnHs.7q2US39GV6/Hi1ASNLYCr/a8K', NULL, NULL, '2021-07-11 16:52:22', '2022-07-12 06:00:56', '2022-07-12 06:00:56'),
(34, 5, 23, NULL, NULL, NULL, NULL, 'dfvbddfvdf', '$2y$10$frGgsaH7TMbqyP.hzkd1NulDIil0/SDY9ctN5Ahgt6t2dBsMqPvH2', NULL, NULL, '2022-04-05 11:35:25', '2022-04-05 11:35:51', '2022-04-05 11:35:51'),
(35, 5, 24, 'I Putu Agus Eka Putra', 'Gianyar', 'manager@gmail.com', 'download.jpg', 'manager', '$2y$10$fy0kVFX4bnIUZ3La2CB8WOkZp1NBfCz6CTrd2zXJsEax9lVF.25DK', NULL, NULL, '2022-04-12 14:31:12', '2022-07-15 09:13:05', NULL),
(36, 5, 25, 'Kadek Ulandari', 'Gianyar', 'ulandarikdk@gmail.com', '22CDC5D7-DC5D-4F44-B986-D9EDB38EB92A.jpeg', 'kepalagudang', '$2y$10$7pHDGukYZSXbt9hSAWang.DXqq482qq3y.K8qpDzIR1GD4eWL4lr6', NULL, NULL, '2022-04-12 14:33:00', '2022-07-14 10:33:50', NULL),
(37, 127, 26, NULL, NULL, NULL, NULL, 'intiru', '$2y$10$HnEh2hNE/GNF1vjc2Fx9ben5OeJ0ebEhpHQk/CmaGj5XmD2ZTm3jm', NULL, NULL, '2022-04-13 13:48:57', '2022-07-12 06:00:47', '2022-07-12 06:00:47');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_role`
--

CREATE TABLE `tb_user_role` (
  `id` int(11) NOT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  `role_keterangan` text DEFAULT NULL,
  `role_akses` text DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user_role`
--

INSERT INTO `tb_user_role` (`id`, `role_name`, `role_keterangan`, `role_akses`, `updated_at`, `created_at`, `deleted_at`) VALUES
(2, 'ADMINISTRATOR', NULL, '{\"dashboard_admin\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"dashboard_manager\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"dashboard_kepala_gudang\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"role_user\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"user\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"stok_barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":false,\"edit\":false,\"delete\":false}},\"kategori_barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"stok_barang_manager\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"barang_masuk\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"konfirmasi_permintaan_barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"laporan_barang_masuk\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false}},\"laporan_barang_keluar\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false}},\"stok_barang_kepala_gudang\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"permintaan_barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"history_permintaan_barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}}}', '2022-08-07 16:12:42', '2019-05-14 22:20:00', NULL),
(15, 'SERVICE ADVICER', NULL, '', '2020-12-05 12:15:15', '2020-08-02 18:16:33', '2020-12-05 12:15:15'),
(16, 'FOREMAN', NULL, '', '2020-12-05 12:15:20', '2020-08-02 18:16:40', '2020-12-05 12:15:20'),
(17, 'BILLING', NULL, '{\"dashboard\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"appointment\":{\"akses_menu\":true,\"action\":{\"list\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"konsultasi\":{\"akses_menu\":true,\"action\":{\"list\":true,\"consult_done\":true,\"consult_cancel\":true,\"edit\":true,\"history\":true}},\"tindakan\":{\"akses_menu\":true,\"action\":{\"list\":true,\"action_done\":true,\"action_back\":true,\"edit\":true,\"history\":true}},\"kontrol\":{\"akses_menu\":true,\"action\":{\"list\":true,\"control_done\":true,\"control_back\":true,\"edit\":true,\"history\":true}},\"selesai\":{\"akses_menu\":true,\"action\":{\"list\":true,\"medic_record_print\":true,\"payment_print\":true,\"done_back\":true,\"history\":true}},\"payment\":{\"akses_menu\":true,\"action\":{\"list\":true,\"print\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"kalender_jadwal\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"data_pasien\":{\"akses_menu\":true,\"action\":{\"list\":true,\"medic_record_print\":true,\"medic_record_edit\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"reminder\":{\"akses_menu\":true,\"pengaturan_reminder\":{\"akses_menu\":true,\"list\":true,\"update\":true},\"riwayat_reminder\":{\"akses_menu\":true,\"list\":true,\"detail\":true}},\"master_data\":{\"akses_menu\":true,\"staff\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"role_user\":{\"akses_menu\":true,\"list\":true,\"menu_akses\":true,\"create\":true,\"edit\":true,\"delete\":true},\"user\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}}}', '2020-12-05 12:15:26', '2020-08-02 18:16:45', '2020-12-05 12:15:26'),
(18, 'DOKTER', NULL, '{\"dashboard\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"appointment\":{\"akses_menu\":true,\"action\":{\"list\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"konsultasi\":{\"akses_menu\":true,\"action\":{\"list\":true,\"consult_done\":true,\"consult_cancel\":true,\"edit\":true,\"history\":true}},\"tindakan\":{\"akses_menu\":true,\"action\":{\"list\":true,\"action_done\":true,\"action_back\":true,\"edit\":true,\"history\":true}},\"kontrol\":{\"akses_menu\":true,\"action\":{\"list\":true,\"control_done\":true,\"control_back\":true,\"edit\":true,\"history\":true}},\"selesai\":{\"akses_menu\":true,\"action\":{\"list\":true,\"medic_record_print\":true,\"payment_print\":true,\"done_back\":true,\"history\":true}},\"payment\":{\"akses_menu\":true,\"action\":{\"list\":true,\"print\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"kalender_jadwal\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"data_pasien\":{\"akses_menu\":true,\"action\":{\"list\":true,\"medic_record_print\":true,\"medic_record_edit\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"reminder\":{\"akses_menu\":true,\"pengaturan_reminder\":{\"akses_menu\":true,\"list\":true,\"update\":true},\"riwayat_reminder\":{\"akses_menu\":true,\"list\":true,\"detail\":true},\"link_whatsapp\":{\"akses_menu\":true,\"list\":true}},\"master_data\":{\"akses_menu\":true,\"staff\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"role_user\":{\"akses_menu\":true,\"list\":true,\"menu_akses\":true,\"create\":true,\"edit\":true,\"delete\":true},\"user\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"metode_tindakan\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}}}', '2022-07-12 07:56:46', '2020-12-05 12:16:27', '2022-07-12 07:56:46'),
(19, 'PERAWAT', NULL, '{\"dashboard\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"appointment\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false}},\"konsultasi\":{\"akses_menu\":true,\"action\":{\"list\":true,\"consult_done\":true,\"consult_cancel\":true,\"edit\":true,\"detail\":false}},\"tindakan\":{\"akses_menu\":true,\"action\":{\"action_wait_list\":true,\"action_wait_done\":true,\"action_wait_back\":true,\"action_wait_cancel\":true,\"action_wait_edit\":true,\"action_wait_detail\":true,\"action_done_list\":true,\"action_done_edit\":true,\"action_done_detail\":true}},\"kontrol\":{\"akses_menu\":true,\"action\":{\"list\":true,\"control_done\":true,\"control_back\":true,\"edit\":true,\"detail\":false}},\"selesai\":{\"akses_menu\":true,\"action\":{\"list\":true,\"medic_record_print\":true,\"payment_print\":true,\"done_back\":true,\"detail\":false}},\"payment\":{\"akses_menu\":false,\"action\":{\"list\":false,\"print\":false,\"edit\":false}},\"kalender_jadwal\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"data_pasien\":{\"akses_menu\":true,\"action\":{\"list\":true,\"medic_record_print\":true,\"medic_record_edit\":true,\"detail\":false,\"edit\":false,\"delete\":false}},\"reminder\":{\"akses_menu\":true,\"pengaturan_reminder\":{\"akses_menu\":true,\"list\":false,\"update\":true},\"riwayat_reminder\":{\"akses_menu\":false,\"list\":false,\"detail\":false},\"link_whatsapp\":{\"akses_menu\":false,\"list\":false}},\"master_data\":{\"akses_menu\":true,\"staff\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":false},\"role_user\":{\"akses_menu\":false,\"list\":false,\"menu_akses\":false,\"create\":false,\"edit\":false,\"delete\":false},\"user\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false},\"metode_tindakan\":{\"akses_menu\":true,\"list\":true,\"create\":false,\"edit\":false,\"delete\":true}}}', '2022-07-12 06:00:15', '2020-12-05 12:24:43', '2022-07-12 06:00:15'),
(20, 'Apoteker', NULL, '{\"dashboard\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"master_data\":{\"akses_menu\":true,\"satuan\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"jenis_barang\":{\"akses_menu\":true,\"list\":true,\"menu_akses\":true,\"create\":true,\"edit\":true,\"delete\":true},\"supplier\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"pelanggan\":{\"akses_menu\":true,\"list\":true,\"create\":true,\"edit\":true,\"delete\":true},\"role_user\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false},\"user\":{\"akses_menu\":false,\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"pembelian\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"penjualan\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"penyesuaian_stok\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"kartu_stok\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"stok_alert\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"hutang_piutang\":{\"akses_menu\":true,\"hutang_lain_lain\":{\"akses_menu\":true,\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true},\"hutang_supplier\":{\"akses_menu\":true,\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true},\"piutang_lain_lain\":{\"akses_menu\":true,\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true},\"piutang_pelanggan\":{\"akses_menu\":true,\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"laporan\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}}}', '2022-07-12 07:56:58', '2021-07-11 16:51:34', '2022-07-12 07:56:58'),
(21, 'Guru1', NULL, NULL, '2022-04-05 11:34:04', '2022-04-05 11:33:43', '2022-04-05 11:34:04'),
(22, 'Manager', NULL, '{\"dashboard_admin\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"dashboard_manager\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"karyawan\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"role_user\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"user\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"barang_kategori\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"stok_barang_manager\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"barang_masuk\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"konfirmasi_permintaan_barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"laporan_barang_masuk\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"laporan_barang_keluar\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"stok_barang_kepala_gudang\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"permintaan_barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}}}', '2022-04-12 14:30:03', '2022-04-05 11:34:41', '2022-04-12 14:30:03'),
(23, 'Kepala Gudang', NULL, '{\"dashboard_admin\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"dashboard_manager\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"karyawan\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"role_user\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"user\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"barang_kategori\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"stok_barang_manager\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"barang_masuk\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"konfirmasi_permintaan_barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"laporan_barang_masuk\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false}},\"laporan_barang_keluar\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false}},\"stok_barang_kepala_gudang\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"permintaan_barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}}}', '2022-04-12 14:30:09', '2022-04-05 11:34:53', '2022-04-12 14:30:09'),
(24, 'Manager', NULL, '{\"dashboard_admin\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"dashboard_manager\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"dashboard_kepala_gudang\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"role_user\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"user\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"stok_barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"kategori_barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"stok_barang_manager\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"barang_masuk\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"konfirmasi_permintaan_barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"laporan_barang_masuk\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"laporan_barang_keluar\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"stok_barang_kepala_gudang\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"permintaan_barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"history_permintaan_barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}}}', '2022-07-24 21:17:02', '2022-04-12 14:30:20', NULL),
(25, 'Kepala Gudang', NULL, '{\"dashboard_admin\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"dashboard_manager\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"dashboard_kepala_gudang\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"role_user\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"user\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"stok_barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"kategori_barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"stok_barang_manager\":{\"akses_menu\":false,\"action\":{\"list\":false}},\"barang_masuk\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"konfirmasi_permintaan_barang\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create\":false,\"edit\":false,\"delete\":false}},\"laporan_barang_masuk\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false}},\"laporan_barang_keluar\":{\"akses_menu\":false,\"action\":{\"list\":false,\"create_admin\":false,\"create_member\":false,\"detail\":false,\"edit\":false,\"delete\":false}},\"stok_barang_kepala_gudang\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"permintaan_barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"history_permintaan_barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}}}', '2022-08-07 16:11:48', '2022-04-12 14:31:39', NULL),
(26, 'Admin', 'admin', '{\"dashboard_admin\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"dashboard_manager\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"dashboard_kepala_gudang\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"karyawan\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"role_user\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"user\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"barang_kategori\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"stok_barang_manager\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"barang_masuk\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"konfirmasi_permintaan_barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"laporan_barang_masuk\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"laporan_barang_keluar\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create_admin\":true,\"create_member\":true,\"detail\":true,\"edit\":true,\"delete\":true}},\"stok_barang_kepala_gudang\":{\"akses_menu\":true,\"action\":{\"list\":true}},\"permintaan_barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}},\"history_permintaan_barang\":{\"akses_menu\":true,\"action\":{\"list\":true,\"create\":true,\"edit\":true,\"delete\":true}}}', '2022-07-12 07:56:52', '2022-04-13 13:45:58', '2022-07-12 07:56:52'),
(27, 'gngv b', 'f', NULL, '2022-04-13 13:47:31', '2022-04-13 13:46:57', '2022-04-13 13:47:31'),
(28, 'c vdcv', 'dv d', NULL, '2022-04-13 13:47:21', '2022-04-13 13:47:13', '2022-04-13 13:47:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `barang_kategori`
--
ALTER TABLE `barang_kategori`
  ADD PRIMARY KEY (`id_barang_kategori`);

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id_barang_keluar`);

--
-- Indexes for table `barang_keluar_detail`
--
ALTER TABLE `barang_keluar_detail`
  ADD PRIMARY KEY (`id_barang_keluar_detail`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id_barang_masuk`);

--
-- Indexes for table `barang_masuk_detail`
--
ALTER TABLE `barang_masuk_detail`
  ADD PRIMARY KEY (`id_barang_masuk_detail`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `id_karyawan` (`id_karyawan`) USING BTREE,
  ADD KEY `id_user_role` (`id_user_role`) USING BTREE;

--
-- Indexes for table `tb_user_role`
--
ALTER TABLE `tb_user_role`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `barang_kategori`
--
ALTER TABLE `barang_kategori`
  MODIFY `id_barang_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `id_barang_keluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `barang_keluar_detail`
--
ALTER TABLE `barang_keluar_detail`
  MODIFY `id_barang_keluar_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id_barang_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `barang_masuk_detail`
--
ALTER TABLE `barang_masuk_detail`
  MODIFY `id_barang_masuk_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tb_user_role`
--
ALTER TABLE `tb_user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
