<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;


Route::get('/error-test', function () {
    Log::info('calling the error route');
    throw new \Exception('something unexpected broke');
});

Route::get('/maintenance', function () {
    Artisan::call('down');
});

Route::get('/production', function () {
    Artisan::call('up');
});

Route::post('/submit-form', function () {
    //
})->middleware(\Spatie\HttpLogger\Middlewares\HttpLogger::class);

Route::group(['namespace' => 'Barang'], function () {

    Route::group(['middleware' => 'authLogin'], function () {

        Route::get('/barang', "Barang@index")->name('barangList');
        Route::post('/barang/datatable', 'Barang@data_table')->name('barangDataTable');
        Route::get('/barang/create', 'Barang@create')->name('barangCreate');
        Route::post('/barang', "Barang@insert")->name('barangInsert');
        Route::delete('/barang/{id}', "Barang@delete")->name('barangDelete');
        Route::get('/barang/{id}', "Barang@edit_modal")->name('barangEditModal');
        Route::post('/barang/{id}', "Barang@update")->name('barangUpdate');

    });

});

Route::group(['namespace' => 'KategoriBarang'], function () {

    Route::group(['middleware' => 'authLogin'], function () {

        Route::get('/kategori_barang', "KategoriBarang@index")->name('kategoriBarangList');
        Route::post('/kategori_barang/datatable', 'KategoriBarang@data_table')->name('kategoriBarangDataTable');
        Route::get('/kategori_barang/create', 'KategoriBarang@create')->name('kategoriBarangCreate');
        Route::post('/kategori_barang', "KategoriBarang@insert")->name('kategoriBarangInsert');
        Route::delete('/kategori_barang/{id}', "KategoriBarang@delete")->name('kategoriBarangDelete');
        Route::get('/kategori_barang/{id}', "KategoriBarang@edit_modal")->name('kategoriBarangEditModal');
        Route::post('/kategori_barang/{id}', "KategoriBarang@update")->name('kategoriBarangUpdate');

    });

});

Route::group(['namespace' => 'Karyawan'], function () {

    Route::group(['middleware' => 'authLogin'], function () {

        Route::get('/karyawan', "Karyawan@index")->name('karyawanPage');
        Route::post('/karyawan', "Karyawan@insert")->name('karyawanInsert');
        Route::get('/karyawan-list', "Karyawan@list")->name('karyawanList');
        Route::delete('/karyawan/{id}', "Karyawan@delete")->name('karyawanDelete');
        Route::get('/karyawan/{id}', "Karyawan@edit_modal")->name('karyawanEditModal');
        Route::post('/karyawan/{id}', "Karyawan@update")->name('karyawanUpdate');

    });

});


Route::group(['namespace' => 'User'], function () {

    Route::group(['middleware' => 'authLogin'], function () {

        Route::get('/user', "User@index")->name('userPage');
        Route::post('/user', "User@insert")->name('userInsert');
        Route::get('/user-list', "User@list")->name('userList');
        Route::delete('/user/{kode}', "User@delete")->name('userDelete');
        Route::get('/user/modal/{kode}', "User@edit_modal")->name('userEditModal');
        Route::post('/user/{kode}', "User@update")->name('userUpdate');

    });

});

Route::group(['namespace' => 'UserRole'], function () {

    Route::group(['middleware' => 'authLogin'], function () {

        Route::get('/user-role', "UserRole@index")->name('userRolePage');
        Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
        Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
        Route::get('/user-role/modal/{id}', "UserRole@edit")->name('userRoleEditModal');
        Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
        Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
        Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');

    });

});

Route::group(['namespace' => 'StokBarangManager'], function () {

    Route::group(['middleware' => 'authLogin'], function () {

        Route::get('/stok-barang/manager', "StokBarangManager@index")->name('stokBarangManagerList');
        Route::post('/stok-barang/manager/datatable', 'StokBarangManager@data_table')->name('stokBarangManagerDataTable');
    });

});

Route::group(['namespace' => 'StokBarangKepalaGudang'], function () {

    Route::group(['middleware' => 'authLogin'], function () {

        Route::get('/stok-barang/kepala-gudang', "StokBarangKepalaGudang@index")->name('stokBarangKepalaGudangList');
        Route::post('/stok-barang/kepala-gudang/datatable', 'StokBarangKepalaGudang@data_table')->name('stokBarangKepalaGudangDataTable');
    });

});

Route::group(['namespace' => 'BarangMasuk'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/barang-masuk', 'BarangMasuk@index')->name('barangMasukList');
        Route::post('/barang-masuk/datatable', 'BarangMasuk@data_table')->name('barangMasukDataTable');
        Route::get('/barang-masuk/create', 'BarangMasuk@create')->name('barangMasukCreate');
        Route::post('/barang-masuk/faktur', 'BarangMasuk@faktur')->name('barangMasukFaktur');
        Route::post('/barang-masuk/insert', 'BarangMasuk@insert')->name('barangMasukInsert');

        Route::get('/barang-masuk/edit/{id_barang_masuk}', 'BarangMasuk@edit')->name('barangMasukEdit');
        Route::post('/barang-masuk/update/{id_barang_masuk}', 'BarangMasuk@update')->name('barangMasukUpdate');
        Route::get('/barang-masuk/detail/modal/{id_barang_masuk}', 'BarangMasuk@detail_modal')->name('barangMasukDetailModal');

    });

});

Route::group(['namespace' => 'KonfirmasiPermintaanBarang'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/konfirmasi-permintaan-barang', 'KonfirmasiPermintaanBarang@index')->name('konfirmasiPermintaanBarangList');
        Route::post('/konfirmasi-permintaan-barang/datatable', 'KonfirmasiPermintaanBarang@data_table')->name('konfirmasiPermintaanBarangDataTable');
        Route::post('/konfirmasi-permintaan-barang/faktur', 'KonfirmasiPermintaanBarang@faktur')->name('konfirmasiPermintaanBarangFaktur');
        Route::post('/konfirmasi-permintaan-barang/insert', 'KonfirmasiPermintaanBarang@insert')->name('konfirmasiPermintaanBarangInsert');

        Route::get('/konfirmasi-permintaan-barang/edit/{id_barang_keluar}', 'KonfirmasiPermintaanBarang@edit')->name('konfirmasiPermintaanBarangEdit');
        Route::get('/konfirmasi-permintaan-barang/detail/modal/{id_barang_keluar}', 'KonfirmasiPermintaanBarang@detail_modal')->name('konfirmasiPermintaanBarangDetailModal');
        Route::get('/konfirmasi-permintaan-barang/terima/{id_barang_keluar}', "KonfirmasiPermintaanBarang@update_terima")->name('konfirmasiPermintaanBarangUpdateTerima');
        Route::get('/konfirmasi-permintaan-barang/tolak/{id_barang_keluar}', "KonfirmasiPermintaanBarang@update_tolak")->name('konfirmasiPermintaanBarangUpdateTolak');
    });

});

Route::group(['namespace' => 'LaporanBarangMasuk'], function () {
    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/laporan/masuk', 'LaporanBarangMasuk@index')->name('laporanBarangMasukList');
        Route::post('/laporan/barang-masuk/datatable', 'LaporanBarangMasuk@data_table_barang_masuk')->name('laporanBarangMasukDataTable');

        Route::post('/laporan/barang-masuk/cetak/pdf', 'LaporanBarangMasuk@cetak_pdf')->name('laporanBarangMasukCetakPdf');
    });
});

Route::group(['namespace' => 'LaporanBarangKeluar'], function () {
    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/laporan/keluar', 'LaporanBarangKeluar@index')->name('laporanBarangKeluarList');
        Route::post('/laporan/barang-keluar/datatable', 'LaporanBarangKeluar@data_table_barang_keluar')->name('laporanBarangKeluarDataTable');

        Route::post('/laporan/barang-keluar/cetak/pdf', 'LaporanBarangKeluar@cetak_pdf')->name('laporanBarangKeluarCetakPdf');
    });
});

Route::group(['namespace' => 'StokBarangKepalaGudang'], function () {

    Route::group(['middleware' => 'authLogin'], function () {

        Route::get('/stok-barang/kepala-gudang', "StokBarangKepalaGudang@index")->name('stokBarangList');
        Route::post('/stok-barang/kepala-gudang/datatable', 'StokBarangKepalaGudang@data_table')->name('stokBarangKepalaGudangDataTable');

    });

});


Route::group(['namespace' => 'PermintaanBarang'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/permintaan-barang', 'PermintaanBarang@index')->name('permintaanBarangList');
        Route::post('/permintaan-barang/datatable', 'PermintaanBarang@data_table')->name('permintaanBarangDataTable');
        Route::get('/permintaan-barang/create', 'PermintaanBarang@create')->name('permintaanBarangCreate');
        Route::post('/permintaan-barang/faktur', 'PermintaanBarang@faktur')->name('permintaanBarangFaktur');
        Route::post('/permintaan-barang/insert', 'PermintaanBarang@insert')->name('permintaanBarangInsert');

        Route::get('/permintaan-barang/edit/{id_barang_keluar}', 'PermintaanBarang@edit')->name('permintaanBarangEdit');
        Route::post('/permintaan-barang/update/{id_barang_keluar}', 'PermintaanBarang@update')->name('permintaanBarangUpdate');
        Route::get('/permintaan-barang/detail/modal/{id_barang_keluar}', 'PermintaanBarang@detail_modal')->name('permintaanBarangDetailModal');

    });

});

Route::group(['namespace' => 'HistoryPermintaanBarang'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/history-permintaan-barang', 'HistoryPermintaanBarang@index')->name('historyPermintaanBarangList');
        Route::post('/history-permintaan-barang/datatable', 'HistoryPermintaanBarang@data_table')->name('historyPermintaanBarangDataTable');

        Route::get('/history-permintaan-barang/detail/modal/{id_barang_keluar}', 'HistoryPermintaanBarang@detail_modal')->name('historyPermintaanBarangDetailModal');

    });

});

Route::namespace('General')->group(function () {

    Route::get('undercosntruction', "Underconstruction@index")->name('underconstructionPage');
    Route::post('city/list', "General@city_list")->name('cityList');
    Route::post('subdistrict/list', "General@subdistrict_list")->name('subdistrictList');

    Route::group(['middleware' => 'checkLogin'], function () {
        Route::get('/', "Login@index")->name("loginPage");
        Route::get('/lupa-password', "Login@lupaPassword")->name("lupaPasswordPage");
        Route::post('/lupa-password', "Login@lupaPasswordDo")->name("lupaPasswordDo");
        Route::post('/roles/login', 'Login@roles')->name("rolesLogin");
        Route::post('/login', "Login@do")->name("loginDo");
    });

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/dashboard/admin', "DashboardAdmin@index")->name('dashboardAdminPage');
        Route::get('/whatsapp-test/admin', "DashboardAdmin@whatsapp_test")->name('whatsappTest');

        Route::get('/dashboard/manager', "DashboardManager@index")->name('dashboardManagerPage');
        Route::get('/whatsapp-test/manager', "DashboardManager@whatsapp_test")->name('whatsappTest');

        Route::get('/dashboard/kepala-gudang', "DashboardKepalaGudang@index")->name('dashboardKepalaGudangPage');
        Route::get('/whatsapp-test/kepala-gudang', "DashboardKepalaGudang@whatsapp_test")->name('whatsappTest');

        Route::get('/logout', "Logout@do")->name('logoutDo');
        Route::get('/profile', "Profile@index")->name('profilPage');
        Route::post('/profile/administrator', "Profile@update_administrator")->name('profilAdministratorUpdate');

        Route::post('/cetak/pdf', "General@cetak_pdf")->name('cetakPdf');

    });
});

// =============== Master Data =================== //
//
//Route::group(['namespace' => 'MasterData', 'middleware' => 'authLogin'], function () {
//
//    // Satuan
//    Route::get('/satuan', "Satuan@index")->name('satuanList');
//    Route::post('/satuan', "Satuan@insert")->name('satuanInsert');
//    Route::delete('/satuan/{id}', "Satuan@delete")->name('satuanDelete');
//    Route::get('/satuan/{id}', "Satuan@edit_modal")->name('satuanEditModal');
//    Route::post('/satuan/{id}', "Satuan@update")->name('satuanUpdate');
//
//    // Jenis Barang
//    Route::get('/jenis-barang', "JenisBarang@index")->name('jenisBarangList');
//    Route::post('/jenis-barang', "JenisBarang@insert")->name('jenisBarangInsert');
//    Route::delete('/jenis-barang/{id}', "JenisBarang@delete")->name('jenisBarangDelete');
//    Route::get('/jenis-barang/{id}', "JenisBarang@edit_modal")->name('jenisBarangEditModal');
//    Route::post('/jenis-barang/{id}', "JenisBarang@update")->name('jenisBarangUpdate');
//
//    // Supplier
//    Route::get('/supplier', "Supplier@index")->name('supplierList');
//    Route::post('/supplier', "Supplier@insert")->name('supplierInsert');
//    Route::delete('/supplier/{id}', "Supplier@delete")->name('supplierDelete');
//    Route::get('/supplier/{id}', "Supplier@edit_modal")->name('supplierEditModal');
//    Route::post('/supplier/{id}', "Supplier@update")->name('supplierUpdate');
//
//    // Supplier
//    Route::get('/pelanggan', "Pelanggan@index")->name('pelangganList');
//    Route::post('pelanggan', "Pelanggan@insert")->name('pelangganInsert');
//    Route::delete('/pelanggan/{id}', "Pelanggan@delete")->name('pelangganDelete');
//    Route::get('/pelanggan/{id}', "Pelanggan@edit_modal")->name('pelangganEditModal');
//    Route::post('/pelanggan/{id}', "Pelanggan@update")->name('pelangganUpdate');
//
//    // Karyawan
//    Route::get('/karyawan', "Karyawan@index")->name('karyawanPage');
//    Route::post('/karyawan', "Karyawan@insert")->name('karyawanInsert');
//    Route::get('/karyawan-list', "Karyawan@list")->name('karyawanList');
//    Route::delete('/karyawan/{id}', "Karyawan@delete")->name('karyawanDelete');
//    Route::get('/karyawan/{id}', "Karyawan@edit_modal")->name('karyawanEditModal');
//    Route::post('/karyawan/{id}', "Karyawan@update")->name('karyawanUpdate');
//
//    // User
//    Route::get('/user', "User@index")->name('userPage');
//    Route::post('/user', "User@insert")->name('userInsert');
//    Route::get('/user-list', "User@list")->name('userList');
//    Route::delete('/user/{kode}', "User@delete")->name('userDelete');
//    Route::get('/user/modal/{kode}', "User@edit_modal")->name('userEditModal');
//    Route::post('/user/{kode}', "User@update")->name('userUpdate');
//
//    // Role User
//    Route::get('/user-role', "UserRole@index")->name('userRolePage');
//    Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
//    Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
//    Route::get('/user-role/modal/{id}', "UserRole@edit")->name('userRoleEditModal');
//    Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
//    Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
//    Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');

//    // Metode Tindakan
//    Route::get('/metode-tindakan', "ActionMethod@index")->name('actionMethodPage');
//    Route::post('/metode-tindakan', "ActionMethod@insert")->name('actionMethodInsert');
//    Route::get('/metode-tindakan-list', "ActionMethod@list")->name('actionMethodList');
//    Route::delete('/metode-tindakan/{kode}', "ActionMethod@delete")->name('actionMethodDelete');
//    Route::get('/metode-tindakan/modal/{kode}', "ActionMethod@edit_modal")->name('actionMethodEditModal');
//    Route::post('/metode-tindakan/{kode}', "ActionMethod@update")->name('actionMethodUpdate');
//
//});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    //Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

