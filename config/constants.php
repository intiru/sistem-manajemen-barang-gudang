<?php
/**
 * Created by PhpStorm.
 * User: mahendrawardana
 * Date: 07/02/19
 * Time: 13.36
 */

return [
    'acc_api_token' => 'c890cc0bd6562ed5b3a2a1105d6ddb9af9fdadab0c55216255bb0465d23c604aab845955b7b43787392c108f5a8cc82292435c99e7e93720145d89193cac06ba',
    'prefixProduksi' => 'MKK-',
    'ppnPersen' => 10,
    'decimalStep' => .01,
    'kodeHutangSupplier' => 'HS',
    'kodePreOrder' => 'PO',
    'kodeHutangLain' => 'HL',
    'kodePiutangPelanggan' => 'PP',
    'kodePiutangLain' => 'PL',
    'kodePenyesuaianStokBahan' => 'PSB',
    'kodePenyesuaianStokProduk' => 'PSP',
    'kodeJurnalUmum' => 'JU',
    'kodeAsset' => 'AT',
    'kodeKategoryAsset' => 'KAS',
    'bankType' => 'BCA',
    'bankRekening' => '4161877888',
    'bankAtasNama' => 'BAMBANG PRANOTO',
    'companyName' => 'Sistem Klinik',
    'companyAddress' => 'JL. Jendral A Yani, No. 04, Baler Bale Agung, Negara',
    'companyPhone' => '0818 0260 9624',
    'companyTelp' => '(0365) 41100',
    'companyEmail' => 'kutuskutusbali@gmail.com',
    'companyBendahara' => 'ARNIEL',
    'companyTuju' => 'Bapak Angga',
    'topMenu' => [
        'dashboard_admin' => 'dashboard_admin',
        'dashboard_manager' => 'dashboard_manager',
        'dashboard_kepala_gudang' => 'dashboard_kepala_gudang',

        'barang' => 'stok_barang',
        'kategori_barang' => 'kategori_barang',
        'karyawan' => 'karyawan',
        'role_user' => 'role_user',
        'user' => 'user',

        'stok_barang_manager' => 'stok_barang_manager',
        'barang_masuk' => 'barang_masuk',
        'konfirmasi_permintaan_barang' => 'konfirmasi_permintaan_barang',
        'laporan_barang_masuk'=> 'laporan_barang_masuk',
        'laporan_barang_keluar'=> 'laporan_barang_keluar',

        'stok_barang_kepala_gudang' => 'stok_barang_kepala_gudang',
        'permintaan_barang' => 'permintaan_barang',
        'history_permintaan_barang' => 'history_permintaan_barang',
    ]
];
