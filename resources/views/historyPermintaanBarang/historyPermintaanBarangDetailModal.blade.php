<div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xxlg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Barang Keluar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body detail-info">
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">User Aplikasi</label>
                        <div class="col-9 col-form-label">
                            {{ $barang_keluar->user->nama_karyawan }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">No Faktur</label>
                        <div class="col-9 col-form-label">
                            {{ $barang_keluar->brk_faktur }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Tanggal</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_date($barang_keluar->brk_tanggal) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Keterangan</label>
                        <div class="col-9 col-form-label">
                            {{ $barang_keluar->brk_keterangan }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Status</label>
                        <div class="col-9 col-form-label">
                            {{ $barang_keluar->brk_status }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Penanggung Jawab</label>
                        <div class="col-9 col-form-label">
                            {{ $barang_keluar->brk_penanggung_jawab }}
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Merek Barang</th>
                                <th>Qty</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($barang_keluar->barang_keluar_detail as $key => $row)
                                <tr>
                                    <td>{{ ++$key }}.</td>
                                    <td>{{ $row->barang->brg_kode.' '.$row->barang->brg_nama }}</td>
                                    <td>{{ Main::format_number($row->bkd_qty_keluar) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</div>