<form action="{{ route('barangUpdate', ['id'=>Main::encrypt($edit->id_barang)]) }}" method="post"
      class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Kategori Barang</label>
                            <select class="form-control m-select2" name="id_barang_kategori"
                                    data-placeholder="Pilih Kategori" style="width: 100%">
                                @foreach($barang_kategori as $row)
                                    <option value="{{ $row->id_barang_kategori}}" {{ $row->id_barang_kategori == $edit->id_barang_kategori ? 'selected' : ''  }}>
                                        {{ $row->brk_kode }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Kode Barang</label>
                            <input type="text" class="form-control m-input" name="brg_kode"
                                   value="{{ $edit->brg_kode }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Merek Barang</label>
                            <input type="text" class="form-control m-input" name="brg_nama"
                                   value="{{ $edit->brg_nama }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Stok Barang</label>
                            <input type="number" class="form-control m-input" name="brg_stok"
                                   value="{{ $edit->brg_stok }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Satuan Barang</label>
                            <input type="text" class="form-control m-input" name="brg_satuan"
                                   value="{{ $edit->brg_satuan }}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>