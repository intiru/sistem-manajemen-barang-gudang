@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            {!! $date_filter !!}

            <div class="m-portlet m-portlet--bordered-semi m-portlet--space m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <button type="button"
                                        class="btn btn-success m-btn--pill cetak-pdf"
                                        data-url="{{ route('laporanBarangKeluarCetakPdf') }}"
                                        data-data="{{ json_encode($table_data_post) }}">
                                    <i class="la la-print"></i> Cetak Laporan
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">

                    <div class="m-widget25">
                        <div class="m-widget25--progress">
                            <div class="m-widget25__progress">
                                <span class="m-widget25__progress-number">
                                    {{ \app\Helpers\Main::format_number($barang_keluar_total) }}
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 100%;"
                                         aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget25__progress-sub">
                                    Barang Keluar
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 0">

                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab-barang-keluar" aria-expanded="true">
                        <i class="flaticon-shopping-basket"></i>
                        Barang Keluar
                    </a>
                </li>
            </ul>
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-barang-keluar" role="tabpanel" aria-expanded="true">
                        <div class="m-portlet__body">
                            <table class="table table-bordered datatable-new"
                                   data-url="{{ route('laporanBarangKeluarDataTable') }}"
                                   data-column="{{ json_encode($datatable_column_barang_keluar) }}"
                                   data-data="{{ json_encode($table_data_post) }}">
                                <thead>
                                <tr>
                                    <th width="20">No</th>
                                    <th>No Faktur</th>
                                    <th>Merek Barang</th>
                                    <th>Tanggal</th>
                                    <th>Status</th>
                                    <th>Keterangan</th>
                                    <th>Qty</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection
