<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <style type="text/css">
        body {
            font-size: 11px;
        }

        h1 {
            font-size: 18px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h2 {
            font-size: 16px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji" !important;
        }

        h3 {
            font-size: 14px;
            font-weight: bold;
        }

        h4 {
            font-size: 12px;
            font-weight: bold;
        }

        .label-2 label {
            padding-top: 12px;
        }

        .table th, .table td {
            padding: 2px 4px;
        }

        .table th {
            text-align: center;
        }

        .table th.number, .table td.number {
            text-align: right;
        }

        .bold {
            font-weight: bold;
        }


    </style>
    <?php
    $width_label = '64';
    ?>

</head>
<body>

<h3 align="center">Laporan Barang Keluar {{ env('BUSINESS_NAME') }}</h3>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<hr/>
<br/>
<h4 align="center">Rangkuman</h4>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Barang Keluar</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>{{ Main::format_number($barang_keluar) }}</th>
    </tr>
    </tbody>
</table>
<br/>
<h4 align="center">Data Barang Keluar</h4>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>No Faktur</th>
        <th>Merek Barang</th>
        <th>Tanggal</th>
        <th>Keterangan</th>
        <th>Qty</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data_barang_keluar as $key => $row)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $row->brk_faktur }}</td>
            <td class="number">{{ Main::format_datetime($row->brk_tanggal) }}</td>
            <td>{{ $row->brg_kode . ' ' . $row->brg_nama }}</td>
            <td>{{ $row->brk_keterangan }}</td>
            <td class="number">{{ Main::format_number($row->bkd_qty_keluar) }}</td>
            <td>{{ Main::barang_tipe($row->brk_tipe) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<br/>
<br/>
<table width="100%">
    <tr>
        <td width="50%" align="center">Yang Bertanggung Jawab</td>
        <td width="50%" align="center">Menyetujui</td>
    </tr>
    <tr>
        <td height="70"></td>
        <td height="70"></td>
    </tr>
    <tr>
        <td align="center"><u>{{ $kepala_gudang_nama }}</u></td>
        <td align="center"><u>{{ $manager_nama }}</u></td>
    </tr>
    <tr>
        <td align="center">Kepala Gudang</td>
        <td align="center">Manager</td>
    </tr>
</table>

</body>

