<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Login | Sistem Management Gudang</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}"/>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-12 text-center" style="padding-top: 20px; margin-bottom: 30px">
            <img src="{{ asset('images/logo-management-gudang.png') }}" height="100" style="margin-bottom: 20px">
            <h4>Alamat : Kompleks Pertokoan Astina Timur Gianyar & Jl. Mulawarman No. 127 Abianbase Kec. Gianyar. No. Telepon 081999957000 & 08199956000</h4>

        </div>
    </div>
</div>
<div style="background-image: url('{{ asset('images/login-wallpaper.jpg') }}'); background-position: 0px center; background-size: 100%; padding: 200px 30px 20px 30px">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3">
                <div class="m-login__signin" style="background-color: white; padding: 10px; border-radius: 10px">
                    <div class="m-login__head">
                        <h2 class="m-login__title text-center">LOGIN ADMINISTRATOR</h2>
                        <br />
                    </div>
                    <form class="m-login__form m-form form-send" action="{{ route('loginDo') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group m-form__group">
                            <input class="form-control m-input" type="text" placeholder="Username"
                                   name="username" autocomplete="off">
                        </div>
                        <div class="form-group m-form__group">
                            <input class="form-control m-input m-login__form-input--last" type="password"
                                   placeholder="Password" name="password">
                        </div>
                        <div class="m-login__form-action text-center">
                            <br />
                            <button type="submit"
                                    class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air btn-login">
                                LOGIN
                            </button>
                            <br />
                            <br />
                            <br />
                            <a href=" {{ route('lupaPasswordPage') }}">Klik untuk Lupa Password</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>

<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>

</body>

</html>
