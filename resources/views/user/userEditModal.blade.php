<form action="{{ route('userUpdate', ['id'=>Main::encrypt($edit->id)]) }}" method="post" class="m-form form-send">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <h3>Data Karyawan: </h3>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Karyawan </label>
                            <input type="text" class="form-control m-input" name="nama_karyawan" value="{{ $edit->nama_karyawan }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Alamat Karyawan </label>
                            <input type="text" class="form-control m-input" name="alamat_karyawan" value="{{ $edit->alamat_karyawan }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Email Karyawan </label>
                            <input type="text" class="form-control m-input" name="email_karyawan" value="{{ $edit->email_karyawan }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Foto Karyawan </label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-success btn-file">
                                        <i class="fa fa-file-image"></i> Browse Foto
                                        <input type="file"
                                               class="imgInp"
                                               name="foto_karyawan"
                                               accept="image/*">
                                    </span>
                                </span>
                                <input type="text" class="form-control" readonly>
                            </div>
                            <img src="{{ asset('upload/'.$edit->foto_karyawan) }}" width="200">
                        </div>
                        <br />
                        <h3>Akun Karyawan:</h3>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">User Role </label>
                            <select name="id_user_role" class="form-control m-select2" style="width: 100%">
                                <option value="">Pilih</option>
                                @foreach($user_role as $r)
                                    <option value="{{ $r->id }}" {{ $edit->id_user_role == $r->id ? 'selected':'' }}>{{ $r->role_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Username </label>
                            <input type="text" class="form-control m-input" name="username" value="{{ $edit->username }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Password </label>
                            <input type="password" class="form-control m-input" name="password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>