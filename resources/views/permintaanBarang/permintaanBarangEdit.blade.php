@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">
        .input-group.bootstrap-touchspin > .input-group-btn {
            display: none !important;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/pembelian.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    <form action="{{ route('permintaanBarangUpdate', ['id_barang_keluar'=>Main::encrypt($edit->id_barang_keluar)]) }}"
          method="post"
          style="width: 100%"
          data-redirect="{{ route('permintaanBarangList') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          data-confirm-status="false"
          data-confirm-message="Data akan masuk ke stok barang, jika terjadi kesalahan input jumlah barang, silahkan perbaiki di <strong>Menu Penyesuaian Stok</strong>"
          class="form-send">

        {{ csrf_field() }}

        <input type="hidden" name="urutan" value="">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Barang
                                    Masuk</label>
                                <div class="col-6">
                                    <input type="text"
                                           class="form-control m_datetimepicker"
                                           readonly=""
                                           name="tanggal"
                                           placeholder="Select date &amp; time"
                                           value="{{ date('d-m-Y H:i', strtotime($edit->brk_tanggal)) }}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Status</label>
                                <div class="col-6">
                                    <select name="tipe" class="form-control m-select2">
                                        <option value="barang_keluar" {{ $edit->brk_tipe == 'barang_keluar' ? 'selected':'' }}>
                                            Barang Keluar
                                        </option>
                                        <option value="barang_rusak" {{ $edit->brk_tipe == 'barang_rusak' ? 'selected':'' }}>
                                            Barang Rusak
                                        </option>
                                        <option value="permintaan_barang" {{ $edit->brk_tipe == 'permintaan_barang' ? 'selected':'' }}>
                                            Permintaan Barang
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label required">Penanggung Jawab</label>
                                <div class="col-6">
                                    <input type="text" name="brk_penanggung_jawab" class="form-control"
                                           value="{{ $edit->brk_penanggung_jawab }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-6 col-form-label required">
                                    <strong>No Faktur Barang Masuk</strong>
                                </label>
                                <div class="col-6">
                                    <input type="text" name="brk_faktur" class="form-control"
                                           value="{{ $edit->brk_faktur }}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-6 col-form-label">Keterangan/Catatan</label>
                                <div class="col-6">
                                    <textarea class="form-control"
                                              name="keterangan">{{ $edit->brk_keterangan }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <table class="table-bahan table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th>Merek Barang</th>
                                <th>Qty</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($edit->barang_keluar_detail as $key => $row)
                                <tr data-index="{{ ++$key }}">
                                    <td class="m--hide">
                                        <input type="hidden" name="id_barang_keluar_detail[]"
                                               value="{{ $row->id_barang_keluar_detail }}">
                                        <input type="hidden" name="id_barang[]" value="{{ $row->id_barang }}">
                                        <input type="hidden" name="qty_keluar[]" value="{{ $row->bkd_qty_keluar }}">
                                    </td>
                                    <td>{{ $row->barang->brg_kode.' '.$row->barang->brg_nama }}</td>
                                    <td>{{ Main::format_number($row->bkd_qty_keluar) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>


        <div class="produksi-buttons">
            <button type="submit"
                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan Barang Masuk</span>
                        </span>
            </button>

            <a href="{{ route('permintaanBarangList') }}"
               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
            </a>
        </div>

    </form>
@endsection