@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">
        .input-group.bootstrap-touchspin > .input-group-btn {
            display: none !important;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/pembelian.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    @include('permintaanBarang/modalBarang')
    {{--    @include('permintaanBarang/permintaanBarang/modalBahanStok')--}}
    @include('permintaanBarang/trBarang')

    <form action="{{ route('permintaanBarangInsert') }}"
          method="post"
          style="width: 100%"
          data-redirect="{{ route('permintaanBarangList') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          data-confirm-status="true"
          data-confirm-message="<strong>Yakin Ingin Menambahkan Permintaan Barang</strong>"
          class="form-send">

        {{ csrf_field() }}

        <input type="hidden" name="urutan" value="">
        <input type="hidden" name="faktur">

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Barang Keluar</label>
                                <div class="col-6">
                                    <input type="text"
                                           class="form-control m_datetimepicker"
                                           readonly=""
                                           name="tanggal"
                                           placeholder="Select date &amp; time"
                                           value="{{ date('d-m-Y H:i') }}">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Status</label>
                                <div class="col-6">
                                    <select name="tipe" class="form-control m-select2">
                                        <option value="barang_keluar">Barang Keluar</option>
                                        <option value="barang_rusak">Barang Rusak</option>
                                        <option value="permintaan_barang">Permintaan Barang</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label required">Penanggung Jawab</label>
                                <div class="col-6">
                                    <input type="text" name="brk_penanggung_jawab" class="form-control">
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label required">
                                    <strong>No Faktur</strong>
                                </label>
                                <div class="col-6">
                                    <input type="text" name="brk_faktur" class="form-control" value="">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan/Catatan</label>
                                <div class="col-6">
                                    <textarea class="form-control" name="keterangan"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">

                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <button type="button" class="btn-bahan-row-add btn btn-accent m-btn--pill">
                                        <i class="la la-plus"></i> Tambah Barang
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-bahan table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th>Merek Barang</th>
                                <th>Qty</th>
                                <th width="100">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>


        <div class="produksi-buttons">
            <button type="submit"
                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan Permintaan Barang</span>
                        </span>
            </button>

            <a href="{{ route('permintaanBarangList') }}"
               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
            </a>
        </div>

    </form>
@endsection