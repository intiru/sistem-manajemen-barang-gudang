<table class="bahan-row m--hide">
    <tr data-index="">
        <td class="m--hide">
            <input type="hidden" name="id_barang[]">
        </td>
        <td>
            <div class="nama-barang"></div>
            <br />
            <button class="btn-modal-bahan btn btn-accent btn-sm m-btn--pill"
                    type="button">
                <i class="la la-search"></i> Cari
            </button>
        </td>
        <td>
            <input type="text" name="qty_masuk[]" class="form-control m-input input-numeral" value="1">
        </td>
        <td>
            <button type="button" class="btn-bahan-row-delete btn m-btn--pill btn-danger btn-sm"
                    data-confirm="false">
                <i class="la la-remove"></i> Hapus
            </button>
        </td>
    </tr>
</table>